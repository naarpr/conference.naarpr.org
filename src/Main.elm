module Main exposing (..)

import Browser exposing (Document)
import Data exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)



---- MODEL ----


type alias Model =
    {}


init : ( Model, Cmd Msg )
init =
    ( {}, Cmd.none )



---- UPDATE ----


type Msg
    = NoOp


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    ( model, Cmd.none )



---- VIEW ----


navbar : Html Msg
navbar =
    nav [ class "dark" ]
        [ ul []
            [ li [] [ a [ href "#about" ] [ text "About" ] ]
            , li [] [ a [ href "#resolutions" ] [ text "Resolutions" ] ]
            , li [] [ a [ href "#agenda" ] [ text "Program" ] ]
            , li [] [ a [ href "#logistics" ] [ text "FAQ" ] ]
            , li [] [ a [ href "#attend" ] [ text "Attend" ] ]
            ]
        ]


heroButtons : Html Msg
heroButtons =
    ul []
        [ li [] [ a [ href "#attend" ] [ button [] [ text "Attend" ] ] ]
        , li [] [ a [ href "#about" ] [ button [] [ text "Learn" ] ] ]
        , li [] [ a [ href "#speakers" ] [ button [] [ text "Speakers" ] ] ]
        , li [] [ a [ href "#resolutions" ] [ button [] [ text "Resolutions" ] ] ]
        ]


insertLink : String -> String -> String -> List (Html Msg)
insertLink linkText linkTarget string =
    List.intersperse
        (a [ href linkTarget ] [ text linkText ])
        (List.map text (String.split linkText string))


hero : Html Msg
hero =
    header [ id "top", class "dark" ]
        [ h1 [] [ text siteInfo.title ]
        , p [] (insertLink "National Alliance Against Racist & Political Repression" siteInfo.homeLink siteInfo.tagline)
        , heroButtons
        ]


aboutSection : Html Msg
aboutSection =
    section [ id "about", class "dark" ]
        [ h2 [] [ text "About" ]
        , p []
            ([ img [ src aboutInfo.imgSrc, alt aboutInfo.imgAlt ] [] ]
                ++ insertLink "National Alliance Against Racist & Political Repression" siteInfo.homeLink aboutInfo.text
            )
        , a [ href "https://naarpr.org/updates/50-anniversary-conference-resolutions/" ] [ button [] [ h3 [] [ text "Read the Resolutions" ] ] ]
        ]


sponsorItem : { name : String, imgSrc : String } -> Html Msg
sponsorItem sponsor =
    li []
        [ img
            [ src sponsor.imgSrc
            , alt sponsor.name
            , title sponsor.name
            ]
            []
        ]


endorsementsLink : Html Msg
endorsementsLink =
    node "big"
        []
        [ em []
            [ text "Full list of endorsements: "
            , a [ href siteInfo.homeLink ] [ text siteInfo.homeLink ]
            ]
        ]


endorsementsSection : Html Msg
endorsementsSection =
    section [ id "resolutions" ]
        [ h2 [] [ text "Resolutions" ]

        -- , a [ href "https://forms.gle/tayXF1DcFbuqrySa8" ] [ button [] [ text "Endorse the Call" ] ]
        , p []
            [ node "big"
                []
                [ em []
                    [ text "NAARPR branches and affiliates passed over a dozen resolutions at the conference."
                    ]
                ]
            ]

        -- , ul [] (List.map sponsorItem endorsements)
        , a [ href "https://naarpr.org/updates/50-anniversary-conference-resolutions/" ] [ button [] [ text "Read the Resolutions" ] ]
        ]


speakerItem : { name : String, bio : String, imgSrc : Maybe String } -> Html Msg
speakerItem speaker =
    let
        picture =
            case speaker.imgSrc of
                Nothing ->
                    []

                Just imgUrl ->
                    [ img
                        [ src imgUrl
                        , alt speaker.name
                        , title speaker.name
                        ]
                        []
                    ]
    in
    li []
        [ figure []
            (picture
                ++ [ figcaption []
                        [ h3 [] [ text speaker.name ]
                        , p [] [ text speaker.bio ]
                        ]
                   ]
            )
        ]


speakersSection : Html Msg
speakersSection =
    section [ id "speakers" ]
        [ h2 [] [ text "Speakers" ]
        , ul [] (List.map speakerItem speakers)
        ]


logistics : List { q : String, a : List (Html Msg) }
logistics =
    [ { q = "What lodging is available?"
      , a =
            [ text "There are a limited number of discounted (2-bed) hotel rooms available from the Crowne Plaza Chicago West Loop, roughly 2 miles from the conference venue. Please contact "
            , a [ href "mailto:shasta@naarpr.org" ] [ text "Shasta" ]
            , text " with your conference registration info for the booking code or direct reservation link."
            ]
      }
    , { q = "How can other organizations support the alliance?"
      , a =
            [ text "Organizations can send members to the conference and "
            , a [ href "https://forms.gle/tayXF1DcFbuqrySa8" ] [ text "endorse the call for the 50ᵗʰ Anniversary Conference" ]
            , text " to show support. You may also purchase space in our printed souvinir journal for the confererence by "
            , a [ href "mailto:sydney@naarpr.org" ] [ text "contacting Syd" ]
            , text " for details."
            ]
      }
    , { q = "I can't attend the conference, how else can I help?"
      , a =
            [ text "You can share our posts on social media ("
            , a [ href "https://fb.me/naarpr" ] [ text "facebook" ]
            , text ", "
            , a [ href "https://twitter.com/naarpr" ] [ text "twitter" ]
            , text ", & "
            , a [ href "https://instagram.com/NationalAARPR" ] [ text "instagram" ]
            , text ") and even "
            , a [ href "https://dub.sh/naarpr50th" ] [ text "donate to support the alliance" ]
            ]
      }
    , { q = "Are there SARS-CoV-2 (COVID-19) protocols in place?"
      , a = [ text "In an effort to maintain the safety of all conference participants, we are asking that attendees get an at-home or PCR test ahead of attending the conference. We are also asking attendees to wear a face mask for the duration of the conference. We will have face masks to distribute to those who need one."]
      }
    , { q = "Is there parking and/or public transportation?"
      , a =
            [ p []
                [ text "Yes! See the "
                , a [ href "#attend" ] [ text "\"Attend\" section" ]
                , text " for details."
                ]
            ]
      }
    , { q = "Is the conference venue accessible?"
      , a =
            [ text "The Chicago Teachers Union Hall is an accessible building."
            ]
      }
    , { q = "What food will be available near the conference?"
      , a =
            [ text "We will add recommendations soon. Please check back later."
            ]
      }
    , { q = "Will there be childcare available during the conference?"
      , a =
            [ text "We will add childcare information soon. Please check back later." ]
      }
    , { q = "Is there a deadline to register for the conference?"
      , a =
            [ text "The online pre-registration deadline is Friday, November 3ʳᵈ at 4:00pm. Additional registrations will be offered at the door while capacity allows." ]
      }
    ]


logisticsSection : Html Msg
logisticsSection =
    section [ id "logistics" ]
        [ h2 [] [ text "Logistics (FAQ)" ]
        , ul []
            (List.map
                (\item ->
                    li []
                        [ strong []
                            [ text item.q ]
                        , p
                            []
                            item.a
                        ]
                )
                logistics
            )
        ]


quoteSection : Html Msg
quoteSection =
    section [ id "quote", class "dark" ] [ p [] [ text quoteText ] ]


streetAddress : List (Html Msg)
streetAddress =
    List.intersperse (br [] []) (List.map text geoLocation.address)


attendSection : Html Msg
attendSection =
    section [ id "attend", class "dark" ]
        [ h2 [] [ text "Attend" ]
        , a [ href "https://dub.sh/naarpr50rsvp" ] [ button [] [ h3 [] [ text "Register Now" ] ] ]
        , p [] [ text "Please register by Wednesday, October 25ᵗʰ. Additional registrations will be offered at the door while capacity allows." ]
        , h3 [] [ text "Location & Directions" ]
        , address [] [ a [ href geoLocation.geoUri ] streetAddress ]
        , p [] [ em [] [ text "This is an accessible space." ] ]
        , iframe [ src geoLocation.mapSrc ] []
        , a [ href geoLocation.mapUrl ] [ text "View larger map." ]
        , h3 [] [ text "Public Transit" ]
        , p []
            [ text
                "The CTU hall is on N. Damen Ave. The #50 CTA bus runs on Damen: "
            , a [ href "https://www.transitchicago.com/bus/50/" ]
                [ text "https://www.transitchicago.com/bus/50/" ]
            ]
        , p []
            [ text
                "Grand Ave. is the nearest major east west street, three blocks north of Carroll Ave. The bus line there is #65: "
            , a [ href "https://www.transitchicago.com/bus/65/" ]
                [ text "https://www.transitchicago.com/bus/65/" ]
            ]
        , p []
            [ text
                "If you come from O'Hare on the Blue Line ("
            , a [ href "https://www.transitchicago.com/blueline/" ]
                [ text "https://www.transitchicago.com/blueline/" ]
            , text
                ") you would transfer to the #50 bus at Damen stop"
            , a [ href "https://www.transitchicago.com/station/damo/" ]
                [ text "https://www.transitchicago.com/station/damo/" ]
            ]
        , p []
            [ text
                "If you come from Midway on the Orange Line ("
            , a [ href "https://www.transitchicago.com/orangeline/" ]
                [ text "https://www.transitchicago.com/orangeline/" ]
            , text ") the fastest way is to transfer to the #50 bus at 13th and Archer: "
            , a [ href "https://www.transitchicago.com/station/35or/" ]
                [ text "https://www.transitchicago.com/station/35or/" ]
            ]
        , p []
            [ text
                "If you come from the Holiday Inn on West Harrison on the Blue Line, you would transfer to the #50 bus at the Illinois Medical District stop: "
            , a [ href "https://www.transitchicago.com/station/ilmd/" ]
                [ text "https://www.transitchicago.com/station/ilmd/" ]
            ]
        , p []
            [ text
                "If you come from the Inn of Chicago, take the #65 bus on Grand: "
            , a [ href "https://www.transitchicago.com/bus/65/" ]
                [ text "https://www.transitchicago.com/bus/65/" ]
            ]
        , p []
            [ text
                "If you come from the Crowne Plaza, take the #20 bus ("
            , a [ href "https://www.transitchicago.com/bus/20/" ]
                [ text "https://www.transitchicago.com/bus/20/" ]
            , text ") to Damen and then transfer to the #50 bus."
            ]
        , h3 [] [ text "Parking" ]
        , p []
            [ text "CTU has free parking spaces for 220 cars (140 in 3 lots and 70-80 on Carroll Street). Parking areas are outlined in yellow below."
            , br [] []
            , text "Note: there is a large parking lot across Carroll Street from the West Lot. "
            , strong [] [text "DO NOT PARK THERE! YOU WILL BE TOWED!"
            , text "The Do Not Park lot is outlined in red."]
            ]
        , img [ src "%PUBLIC_URL%/images/parking.jpg", style "max-width" "100%" ] []
        ]


footerSection : Html Msg
footerSection =
    footer [ id "end", class "dark" ] [ navbar ]


agendaSession : Session -> Html Msg
agendaSession session =
    let
        speakers =
            case session.people.speakers of
                Just people ->
                    [ p []
                        [ strong [] [ text "Speaker(s): " ]
                        , text
                            (String.concat (List.intersperse ", " people))
                        ]
                    ]

                Nothing ->
                    []

        moderators =
            case session.people.moderators of
                Just people ->
                    [ p []
                        [ strong [] [ text "Moderator(s): " ]
                        , text
                            (String.concat (List.intersperse ", " people))
                        ]
                    ]

                Nothing ->
                    []
    in
    li []
        ([ h3 [] [ text session.title ]
         , text session.sessionDescription
         ]
            ++ speakers
            ++ moderators
            ++ [ em [] [ text session.sessionLocation ]
               ]
        )


agendaSlot : AgendaSlot -> Html Msg
agendaSlot slot =
    li []
        [ p []
            [ time [] [ text slot.slotTime ]
            , strong [] [ text slot.slotType ]
            , ul [] (List.map agendaSession slot.sessions)
            ]
        ]


agendaDay : { day : String, slots : List AgendaSlot } -> List (Html Msg)
agendaDay daySchedule =
    [ h3 [] [ text daySchedule.day ]
    , ul [] (List.map agendaSlot daySchedule.slots)
    ]


agendaSection : Html Msg
agendaSection =
    section [ id "agenda", class "dark" ]
        ([ h2 [] [ text "Program" ]
         , p []
            [ strong [] [ text "More details coming soon!" ]
            ]
         ]
            ++ List.concat (List.map agendaDay agenda)
        )


view : Model -> Document Msg
view model =
    { title = siteInfo.title
    , body =
        [ hero
        , navbar
        , main_ []
            [ aboutSection
            , speakersSection
            , agendaSection
            , endorsementsSection
            , attendSection
            , quoteSection
            , logisticsSection
            ]
        , footerSection
        ]
    }



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.document
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = always Sub.none
        }
