module Data exposing (..)

---- HEADING ----


siteInfo : { title : String, tagline : String, homeLink : String }
siteInfo =
    { title = "50ᵗʰ Anniversary Conference"
    , tagline = "The National Alliance Against Racist & Political Repression marches onward. Decades of struggle. Victories ahead."
    , homeLink = "https://naarpr.org"
    }


geoLocation : { address : List String, mapSrc : String, mapUrl : String, geoUri : String }
geoLocation =
    { address = [ "CTU Hall", "1901 W. Carroll Avenue", "Chicago, IL 60612" ]
    , mapSrc = "https://www.openstreetmap.org/export/embed.html?bbox=-87.67702996730804%2C41.88651808028945%2C-87.6734894514084%2C41.88836311585891&amp;layer=mapnik&amp;marker=41.88744060473486%2C-87.67525970935822"
    , mapUrl = "https://www.openstreetmap.org/way/209829229"
    , geoUri = "geo:41.88744,-87.67559?z=19"
    }



---- MAIN ----


aboutInfo : { text : String, imgSrc : String, imgAlt : String }
aboutInfo =
    { text = "The 50ᵗʰ Anniversary Conference of the National Alliance Against Racist & Political Repression took place in Chicago, Illinois on November 3-5, 2023. Activists and organizers from across the country gathered to consolidate our gains and plan the further expansion and political sharpening of our ranks."
    , imgSrc = "%PUBLIC_URL%/images/aboutBg.jpg"
    , imgAlt = "Angela Y. Davis, Frank Chapman, and Rasmea Odeh"
    }


quoteText : String
quoteText =
    "“In the long run, the people are our only appeal. The only ones who can free us are ourselves.”  — Assata Shakur"


speakers : List { name : String, bio : String, imgSrc : Maybe String }
speakers =
    [ { name = "Stacy Davis Gates"
      , bio = """Stacy Davis Gates is President of the Chicago Teachers Union, Executive Vice
President of the Illinois Federation of Teachers, Vice President of the American
Federation of Teachers, and Chair of United Working Families. She has spearheaded statewide legislative campaigns that enacted an elected school
board for Chicago, bringing democracy to the district for the first time ever. Her
legislative efforts also won the strongest charter school accountability measures in the
country, restored full bargaining rights that had been denied to Chicago educators for
nearly three decades and helped secure millions of dollars in extra state funding for
Chicago Public Schools."""
      , imgSrc = Just "%PUBLIC_URL%/images/people/stacydavisgates.jpg"
      }
    , { name = "Mayor Brandon Johnson"
      , bio = """Mayor Brandon Johnson is a husband and a father of three based on the West side of Chicago. He is a former teacher, former organizer and a long-time ally of the movement for community control of the police. In 2015, he participated in the hunger strike to prevent the closure of Walter H. Dyett High School. In his election night acceptance speech, he cited the alliance between the civil rights movement and the labor movement as the key to his victory. His campaign was connected closely with CAARPR's work to elect District Councilors committed to holding police accountable, and his public safety plan includes collaboration with them now that they are elected. He was opposed by a candidate supported by the Fraternal Order of Police, and won on a platform of affordable housing, youth employment, education, and racial justice."""
      , imgSrc = Just "%PUBLIC_URL%/images/people/brandonjohnson.jpg"
      }
    , { name = "Angela Y. Davis, PhD"
      , bio = """Angela Davis is an American political activist, academic scholar, and author from Birmingham, Alabama.
She emerged as a prominent counterculture activist during the late 1960s while working with the U.S.
Communist Party and the Black Panther Party. In August 1970 at a Marin County California court room, Jonathan
Jackson and others took Judge Harold J. Haley, a district attorney, and several jury members hostage in an
attempt to publicize prison conditions and state abuses against the Soledad Brothers. This situation resulted in
the deaths of Judge Haley, Jonathan Jackson, James McClain, and William Christmas. Despite Davis not being in
the area at the time, the police named her an accomplice to the crime. 2 months later, she was captured and
imprisoned. Davis was acquitted of all charges on June 4, 1972. Upon release, she called for a national alliance to
free other political prisoners. This is what lead to the National Alliance Against Racist &amp; Political Repression
(NAARPR). In the 40+ years since her release, Davis has been a prolific writer on the themes of women’s rights
and leadership, prisoners’ rights, liberation politics, anti-racism, gender equity, and cultural studies. Some of
Davis’s published work includes: Women, Race &amp; Class, Are Prisons Obsolete? and Freedom is a Constant
Struggle: Ferguson, Palestine and the Foundations of a Movement."""
      , imgSrc = Just "%PUBLIC_URL%/images/people/angeladavis.jpg"
      }
    , { name = "Alderwoman Jeanette Taylor"
      , bio = """Alderwoman Jeanette Taylor is a tireless champion of the people based on the South side in Chicago's 20th ward. One of the earliest supporters of CPAC and a leading force behind ECPS, she has been instrumental in the gains made by the movement to stop police crimes. She not only advocates for the needs of the community, she brings the people with her wherever she goes. She participated in the hunger strike to save Walter H. Dyett High School in 2015, and served on her Local School Council before being elected Alderwoman in 2019. She worked with CAARPR during both of her elections and supports the struggle for community control of the police."""
      , imgSrc = Just "%PUBLIC_URL%/images/people/jeanettetaylor.jpg"
      }
    , { name = "Alderman Carlos Ramirez-Rosa"
      , bio = """Alderman Carlos Ramirez-Rosa represents the 35th ward on Chicago's Northwest side. He has stood with the movement for community control of the police since the fight for CPAC began. Whenever there is a call to defend the gains the people have made, he is among the first to answer it. His work was integral to the passage of ECPS, and he's a long-time ally of the Alliance. He's a fighter for the people who has secured countless progressive victories, from affordable housing to immigrant rights. Before being elected alderman, he served on his Local School Council and worked as a union staffer."""
      , imgSrc = Just "%PUBLIC_URL%/images/people/carlosramirezrosa.png"
      }
    , { name = "Frank Chapman"
      , bio = """Frank Chapman was wrongfully convicted of murder and armed robbery in 1961 and sentenced to life and 50
years in the Missouri State Prison. In 1973, the National Alliance Against Racist & Political Repression (NAARPR)
took up Frank’s case. In 1976 he was released after being incarcerated for 15 years. In 1983, he was elected
Executive Director of NAARPR. He worked alongside Charlene Mitchell, who preceded him as Executive Director
of NAARPR, and with Angela Davis on building an international campaign to free Rev. Ben Chavis and the
Wilmington Ten, Joann Little, Geronimo Pratt, Leonard Peltier and others falsely accused and politically
persecuted. Currently, Chapman is the Field Organizer and Educational Director of the Chicago Alliance Against
Racist & Political Repression (CAARPR). He believes in the inalienable democratic right of Black people to
determine who polices their communities and how their communities are policed. Chapman recently published
his autobiography The Damned Don’t Cry: Pages from the Life of a Black Prisoner & Organizer."""
      , imgSrc = Just "%PUBLIC_URL%/images/people/frankchapman.jpg"
      }
    , { name = "Lennox S. Hinds, J.D. "
      , bio = """Lennox S. Hinds is a Professor Emeritus of Law and former Chair of the Administration of Justice Program,
Rutgers University, New Brunswick, New Jersey. A graduate of The City College of New York and Rutgers Law School, he was
awarded the law school’s J. Skelly Wright Award for contribution to civil rights. He was a Charles H. Revson Fellow, Center
for Legal Education and Urban Policy, City College of New York 1979-1980. He was awarded CCNY’s highest Townsend Harris
Medal for outstanding post-graduate achievement in his chosen field. In addition to his practice as a criminal defense and
international human rights lawyer, he was Nelson Mandela’s US attorney and counsel in the US to the Government of South
Africa, the African National Congress (ANC) of South Africa and (SWAPO) of Namibia. He is the permanent Representative to
the United Nations for the International Association of Democratic Lawyers."""
      , imgSrc = Just "%PUBLIC_URL%/images/people/lennoxshinds.jpg"
      }
    , { name = "Aurelia Ceja"
      , bio = "Aurelia Ceja got their start in immigrant rights organizing in college 7 years ago and has since found a home in anti-police crimes work. They’ve been organizing with the Milwaukee Alliance Against Racist & Political Repression since 2020 and am proud to serve as a co-chair. Aurelia is also the co-chair for the Coalition to March on the RNC in 2024."
      , imgSrc = Just "%PUBLIC_URL%/images/people/aureliaceja.jpg"
      }
    , { name = "Laura Rodriguez"
      , bio = "Laura Rodriguez is one of the Tampa 5 protestors arrested on May 6th at the University of South Florida. They are a Puerto Rican community organizer with the Tampa Bay Community Action Committee and began organizing during the George Floyd Rebellion. They attended the protest on their day off from work to stand in solidarity with SDS against the attacks on higher education, black education and diversity. She firmly stands in opposition to the attacks and to the false charges. Laura was formerly a student organizer with SDS and is a militant fight for the liberation of all oppressed people."
      , imgSrc = Just "%PUBLIC_URL%/images/people/laurarodriguez.jpg"
      }
    , { name = "Angel Naranjo"
      , bio = "Angel is a student, aspiring teacher, and community organizer with Students for a Democratic Society at UIC. Angel first joined the movement in 2021 during their time at Little Village Lawndale High School, where they co-founded LVLHS FightBack! and organized walkouts for black & brown unity against the systemic oppression of the mainly Chicano/Mexican community they were raised in. As a result, Angel developed a revolutionary consciousness and is a proud member of the Freedom Road Socialist Organization. Today, you can find them in the streets and on campus, where they continue to fight for liberation and show solidarity with the movements for immigrant & workers’ rights, to defend ethnic studies, for community control of the police, and to increase Black faculty & enrollment at the University of Illinois at Chicago."
      , imgSrc = Just "%PUBLIC_URL%/images/people/angelnaranjo.jpg"
      }
    , { name = "Keyanna Jones"
      , bio = "Keyanna Jones is a political and social justice activist and community organizer, who is a staunch advocate for quality, affordable childcare and equity in education. She currently works with Community Movement Builders to educate, engage and empower the Black community in Atlanta, Georgia. She is an ordained minister and proprietor of E Equals MC Squared Educational Services, LLC, where she works as a homeschool curriculum consultant, IEP advocate and German translator. Keyanna is the the wife of Jerrod Moore and mother to their 5 unique and extraordinary children."
      , imgSrc = Just "%PUBLIC_URL%/images/people/keyannajones.jpg"
      }
    , { name = "Luis Sifuentes"
      , bio = "A leader in Centro Community Service Organization (Centro CSO), Luis  was born and raised in Los Angeles and is a bus driver for the LA Unified School District and a  member of SEIU Local 99. Luis became involved in the movement against police crimes in 2016 and  joined Centro CSO after the upsurge of fatal shootings by LAPD and LASD against Chicanos in East  LA. Since then, he has been active in the Chicano and anti-war movement and joined the Freedom  Road Socialist Organization (FRSO) to help carry the struggle for socialism for generations to come."
      , imgSrc = Just "%PUBLIC_URL%/images/people/luissifuentes.png"
      }
    , { name = "Jae Yates"
      , bio = "Jae is an organizer in the Twin Cities that does anti-police crimes and mutual aid work. After the Minneapolis Uprising, they co-founded Community Aid Network MN which  provides household supplies and groceries to anyone in the Metro area. During this period, Jae also  wanted to go from just participating in political actions to helping organize mass movement work.  They joined Twin Cities Coalition for Justice for Jamar (TCC4J) to help organize the Taking Back  Pride March in 2020 and has been with the group since. They currently spend most of their time  doing education and outreach around community control of police, working to get CPAC legislation  enshrined in the Minneapolis Charter."
      , imgSrc = Just "%PUBLIC_URL%/images/people/jaeyates.png"
      }
    , { name = "Aislin"
      , bio = "Aislinn is a Co-Executive Director at the Chicago Torture Justice Center, and a long timer organizer who has worked on a variety of campaigns including the Reparations Now movement to pass the historic 2015 Reparations Ordinance for survivors of CPD torture. Born and raised in Chicago, Aislinn founded the Chicago chapter of Black Lives Matter as part of the Freedom Ride to Ferguson in August 2014 and was the youngest founding member of the cultural non-profit that used art for social change, Insight Arts. She is the founder and creator of urban youth magazine, Underground Philosophy as well as a founder of the young women’s performance ensemble dedicated to ending sexual assault, Visibility Now."
      , imgSrc = Just "%PUBLIC_URL%/images/people/aislinnpulley.jpg"
      }
    , { name = "David Jones"
      , bio = "David Jones is an Atlanta- based community organizer with the newly founded Atlanta Alliance Against Racist and Political Repression. They got their start doing student organizing at the University of South Florida in 2019 but during the George Floyd rebellion has been organizing against police crimes in the South. They are currently involved in the fight for justice for the lives lost at the Fulton County jail."
      , imgSrc = Just "%PUBLIC_URL%/images/people/davidjones.jpg"
      }
    , { name = "Xavier \"Xavi\" Velasquez"
      , bio = "Xavi is a unionized worker with The International Association Of Machinists And Aerospace Workers and a Chicano community organizer with La Frontera Nos Cruzó in the Dallas-Fort Worth area. Xavi started organizing with The National Alliance Against Racial And Political Repression-Dallas in 2020 and Co-founded La Frontera Nos Cruzó in 2022. A son of a formerly undocumented immigrant, Xavi saw the need to fight against the kinds of conditions that immigrants to the U.S. faced then and now. Today, Xavi is uniting with the greater Dallas-Fort Worth immigrant community in fighting against Greg Abbott's deadly attacks on immigrants on the border."
      , imgSrc = Just "%PUBLIC_URL%/images/people/xavivelasquez.jpg"
      }
    , { name = "Tionna Jefferson"
      , bio = "Tionna Jefferson is an organizer, artist, and poet. She began organizing in Jacksonville in 2017 with the Students for a Democratic Society at the University of North Florida. From there she got involved in community and National work with the Jacksonville Community Action Committee. She is also a member of the Freedom Road Socialist Organization. It is her goal to empower her community to use their imaginations to envision a better future through people power!"
      , imgSrc = Just "%PUBLIC_URL%/images/people/tionnajefferson.jpg"
      }
    , { name = "B. Neal Jefferson"
      , bio = "B. Neal Jefferson, is an Educator, Poet, and Organizer. Born and raised in Duval County(Jacksonville, FL), as a part of The Jacksonville Community Action Community he fights for community control of the police and to create more safe, equitable, and inclusive city."
      , imgSrc = Just "%PUBLIC_URL%/images/people/bnealjefferson.jpg"
      }
    , { name = "Carlos Montes"
      , bio = "Co-founder of the original Brown Berets & Minister of Information a Chicano militant org, I was a leader and organizer in the historic 1968 ELA Walkouts, & indicted by the grand jury with the ELA 13 for conspiracy to disrupt the school system; won a case. Participated in the first National Chicano Youth Liberation Conference, Denver 1969, where the Plan Espiritual de Aztlan was formulated, calling for self-determination for Chicano Nation. Co-organizer of first Chicano Moratorium in 1969 against the war in Vietnam. In 2006 I helped organize the historic marches against the criminalization of Mexican immigrants. In May 2011 I was arrested at home in an FBI Sheriff’s Swat team on investigation for antiwar work on Iraq and Afghanistan, and solidarity work with Colombia and Palestine struggles. The arrest warrant said: “for providing material support to the PFLP and FARC!” This was part of the FBI attack on FRSO. I have been arrested many times due to my activism and targeted by the FBI for my anti-war and solidarity work. Today leader in FRSO, organize with Centro CSO against LA Sheriffs killings of Blacks and Chicanos. Fight for public education and works with Legalization for All network. March 2023 attended Hugo Chavez 10th year commemoration of his passing in Venezuela."
      , imgSrc = Just "%PUBLIC_URL%/images/people/carlosmontes.jpg"
      }
    , { name = "Gabriel Black Elk"
      , bio = "Gabriel Black Elk is the brother of Paul Castaway, murdered by Denver police in 2015. He is from the Sicangu (pronounced See-chong-goo) Lakota, Burnt Thigh Nation. He and his wife Tonia are lead organizers at Native Lives Matter, a grassroots organization run by and for Indigenous Justice Families, supporting families and raising awareness around the high rate of police killings of Native peoples. They are also the founders of the Remember My Name Foundation which helps assist Impacted Indigenous Families in their fight for justice. And they are water protectors, who spent months at Standing Rock, resisting the Dakota Access Pipeline."
      , imgSrc = Just "%PUBLIC_URL%/images/people/gabrielblackelk.jpg"
      }
    , { name = "Monique Sampson"
      , bio = "Monique Sampson is a community organizer and co-founder of the Jacksonville Community Action Committee and UNF Students for a Democratic Society. She proudly serves as the Treasurer for the National Alliance Against Racist and Political Repression. Monique has been organizing since 2016 and got her start through running militant campaigns to improve the lives of black and brown students on her college campus. During the Summer of 2020, she helped organize three of Jacksonville’s historic marches that took place after the death of George Floyd. Her favorite thing to do is read novels by black authors and spend time with her daughter, husband, three dogs and cat."
      , imgSrc = Just "%PUBLIC_URL%/images/people/moniquesampson.png"
      }
    , { name = "Delilah Pierre"
      , bio = "Delilah Pierre is a Haitian-American, Florida-born (Hillsborough county), Black Trans organizer and activist who has spent years fighting against police brutality and for queer and trans liberation. Her proudest accomplishments include working to stop a police station from being built on the Southside and helping pass the most inclusive conversion therapy ban in Florida's history. She is currently the President of the Tallahassee Community Action Committee as well as a member of the Freedom Road Socialist Organization"
      , imgSrc = Just "%PUBLIC_URL%/images/people/delilahpierre.jpg"
      }
    , { name = "Regina Joseph"
      , bio = "Regina Joseph, (she/her)is a Haitian-American activist. She is also a member of the Freedom Road Socialist Organization. Additionally, Joseph is the President Emeritus of the Tallahassee Community Action Committee with over a decade of experience in student and community organizing in the fight for black liberation and an end to police brutality."
      , imgSrc = Just "%PUBLIC_URL%/images/people/reginajoseph.png"
      }
    , { name = "Michael Sampson II"
      , bio = "Michael Sampson II is the Executive Director and co-founder of the Jacksonville Community Action Committee, a grassroots Black-led organization based in Jacksonville focused on organizing for freedom and self determination. He is currently a Co- Chair with the National Alliance Against Racist and Political Repression along with being a Co-Chair of the African American Commission with the Freedom Road Socialist Organization. Michael is a graduate of Florida State University, with a Bachelor of Sciences degree in Political Science and Social Sciences where he first started organizing around education rights. He was also active in the Occupy Movement. After the murder of Trayvon Martin in 2012, he and others co-founded the Florida based organization, the Dream Defenders. He has been a union organizer for the past decade."
      , imgSrc = Just "%PUBLIC_URL%/images/people/michaelsampson.png"
      }
    , { name = "Jasmine Smith"
      , bio = "Jasmine Smith is a co-chair of the Campaign to Free Incarcerated Survivors of Police Torture and Wrongful Conviction. She is a revolutionist, trend-setter, globalist, activist fighting for human rights and against injustices. She is the driving force behind the campaign to expose Brian P. Forberg, one of the highest paid sergeants in the Chicago Police Department, and to free the Forberg 15, 15 men he framed and coerced."
      , imgSrc = Just "%PUBLIC_URL%/images/people/jasminesmith.png"
      }
    , { name = "Khaleed London"
      , bio = "Khaleed London, Pastor/Director of Shut-Up Prison Ministry. The ministry advocates for those who are imprisoned, formerly imprisoned, and the wrongfully imprisoned, and supports family members. Khaleed London is a longtime conscious citizen of the RNA involved in freedom of all political prisoners and POWs. He works with the Rebuild Collective and the Jericho Movement, Also he and his wife, Makeda were intricately involved in the liberation of the Pontiac Brothers."
      , imgSrc = Just "%PUBLIC_URL%/images/people/khaleedlondon.png"
      }
    , { name = "Sydney Lovіng"
      , bio = "Sydney Lovіng is co-chair of the Dallas Alliance Against Racist and Political Repression, formerly known as North Texas Action Committee (2017-present). She also is a Co-Chair with the NAARPR. A branch of the National Alliance, DAARPR is a DFW-based organization dedicated to fighting police brutality, prison profiteering, and political repression. She has been an activist since she was a student organizing with Decolonize Media Collective, which led the campaign to force the university's divestment from the private prison industry, and organized with BLM 413 and Out Now in Springfield, MA (2014-2017). She works as a kindergarten teacher."
      , imgSrc = Just "%PUBLIC_URL%/images/people/sydlovіng.png"
      }
    , { name = "Chrisley Carpio"
      , bio = "Chrisley Carpio (she/her) is a Filipina immigrant and longtime member and supporter of Students for a Democratic Society (SDS). She was born in Cebu City, Philippines, moved to Miami, Florida, at the age of three and grew up in South Florida. As an undergraduate, she studied history. At the time of the March 6th protest, Chrisley was an admissions evaluator at the University of South Florida (USF) and a member of AFSCME Local 3342. For many years, she has attended SDS protests, marches, and rallies, saying no to tuition increases, budget cuts, layoffs, and cuts to ethnic studies, among other issues. In 2012, she and other protesters shut down the Sanford Police Department to demand justice for Trayvon Martin, and in 2020, she played an active role in organizing protests calling for justice for George Floyd. As a result of her arrest on March 6th, USF fired her in violation of her union contract. She is currently pursuing a grievance for wrongful termination through her union and continuing to fight to get the charges dropped. For fun, she likes to read, knit, bake, and play with her cat, Merlin."
      , imgSrc = Just "%PUBLIC_URL%/images/people/chrisleycarpio.png"
      }
    , { name = "Muhammad Sankari"
      , bio = "Muhammad Sankari is the lead organizer at the Arab American Action Network and a member of the U.S. Palestinian Community Network - Chicago chapter."
      , imgSrc = Just "%PUBLIC_URL%/images/people/muhammadsankari.png"
      }
    , { name = "Anthony Driver"
      , bio = "Anthony Driver, a seasoned political activist and strategist has been politically active since his youth. He is a graduate of Howard University and an advocate for Historical Black Colleges and Universities. After receiving his degree in Political Science and History from Howard, Anthony went on to serve as a Policy Associate for the Estell Group, the first and only Black woman-owned full-service government relations firm at the national level.  Anthony served as a union organizer, lobbyist and political strategist for Service Employees International Union for health care workers in Illinois and Indiana. He worked with CAARPR in the campaign to enact the Ordinance Empowering Communities for Public Safety (ECPS). The ECPS Ordinance was enacted on July 17th , 2021. It was an historic people’s victory and Anthony as a union organizer and community activist made an incredible contribution. Today, he serves as the first ever President of the Community Commission for Public Safety and Accountability and the Executive Director for SEIU IL State Council."
      , imgSrc = Just "%PUBLIC_URL%/images/people/anthonydriver.png"
      }
    , { name = "Dod McColgan"
      , bio = "Dod McColgan is a co-chair with the Chicago Alliance Against Racist and Political Repression and a member of AFSCME Local 1215 as a library worker with the Chicago Public Library. They are also a proud member of Freedom Road Socialist Organization."
      , imgSrc = Just "%PUBLIC_URL%/images/people/dodmccolgan.jpg"
      }
    ]


endorsements : List { name : String, imgSrc : String }
endorsements =
    [ { name = "Arab American Action Network (AAAN)"
      , imgSrc = "%PUBLIC_URL%/images/orgs/aaan.jpg"
      }
    ]


type alias Session =
    { title : String
    , sessionDescription : String
    , sessionLocation : String
    , people :
        { speakers : Maybe (List String)
        , moderators : Maybe (List String)
        }
    }


type alias AgendaSlot =
    { slotTime : String
    , slotType : String
    , sessions : List Session
    }


agenda : List { day : String, slots : List AgendaSlot }
agenda =
    [ { day = "Friday, November 3ʳᵈ"
      , slots =
            [ { slotTime = "6:00 PM"
              , slotType = ""
              , sessions =
                    [ { title = "Doors Open"
                      , sessionDescription = ""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "7:00 PM"
              , slotType = ""
              , sessions =
                    [ { title = "Rally On Human Rights"
                      , sessionDescription = "Defending democracy, fighting back against political repression and community control of the police."
                      , sessionLocation = "Main Hall"
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            ]
      }
    , { day = "Saturday, November 4ᵗʰ"
      , slots =
            [ { slotTime = "8:00 AM"
              , slotType = ""
              , sessions =
                    [ { title = "Doors Open"
                      , sessionDescription = "Registration and continental breakfast."
                      , sessionLocation = "Main Hall"
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "9:00 AM"
              , slotType = "PLENARY"
              , sessions =
                    [ { title = "Welcome"
                      , sessionDescription = "Opening Remarks"
                      , sessionLocation = "Main Hall"
                      , people =
                            { speakers = Just [ "Rasmea Odeh", "Husam Marajda" ]
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "9:30 AM"
              , slotType = "PANEL"
              , sessions =
                    [ { title = "Buliding for Community Control of the Police"
                      , sessionDescription = ""
                      , sessionLocation = ""
                      , people =
                            { speakers = Just [ "Anthony Driver (President of Community Commission for Public Safety and Accountability)", "Dod McColgan (CAARPR - Chicago)", "Jae Yates (TCC4J - Twin Cities)", "Neal Jefferson (Jacksonville Community Action Committee - JAX)", "Syd Loving (NAARPR Dallas)", "Luis Sifunentes (Centro CSO - LA)" ]
                            , moderators = Just [ "Michael Sampson (Jacksonville Community Action Committee - JAX)" ]
                            }
                      }
                    ]
              }
            , { slotTime = "10:35 AM"
              , slotType = "BREAK"
              , sessions =
                    [ { title = "Intermission"
                      , sessionDescription = ""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "10:50 PM"
              , slotType = "PANEL"
              , sessions =
                    [ { title = "Free Our Comrades!"
                      , sessionDescription = "Fighting back against political repression"
                      , sessionLocation = ""
                      , people =
                            { speakers = Just [ "Laura Rodriguez (TBCAC- Tampa 5)", "Aurelia Ceja (Co-chair of MAARPR and Co-chair of the Coalition to March on the RNC - MKE", "Keyanna Jones (Community Movement Builders - ATL)", "Carlos Montes (Centro CSO - LA)" ]
                            , moderators = Just [ "David Jones (Atlanta)" ]
                            }
                      }
                    ]
              }
            , { slotTime = "12:00 PM"
              , slotType = "BREAK"
              , sessions =
                    [ { title = "Lunch"
                      , sessionDescription = ""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "1:00 PM"
              , slotType = "BREAKOUT SESSION"
              , sessions =
                    [ { title = "Attacks on Women and LGBTQ+ Liberation in the Black Belt and Beyond"
                      , sessionDescription = "A panel about the current onslaught of anti-LBGTQ+ and anti-women attacks in the South and beyond. Panelists will discuss defeats and victories regarding reproductive rights, gender affirming care, and laws restricting access to education for LGBTQ+ people and women."
                      , sessionLocation = ""
                      , people =
                            { speakers = Just [ "Quest Riggs (Real Name Campaign - New Orleans)", "Tionna Jefferson (Jacksonville Community Action Committee - JAX)", "Robyn Harbison (MN Abortion Action Committee - Twin Cities)", "Jo Leslie Hargis (NAARPR Dallas)", "Regina Joseph (Tallahassee Community Action Committee - Tallahassee)" ]
                            , moderators = Just [ "Delilah Pierre (Tallahassee Community Action Committee, FRSO Tallahassee)" ]
                            }
                      }
                    , { title = "The Fight for Legalization for All"
                      , sessionDescription = "A workshop facilitated by the Legalization 4 All Network on the border situation & Legalization 4 All campaign, plus a call for immigration groups to join the network."
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Just [ "Angel Naranjo (SDS at UIC - Chicago)", "Xavi Velasquez (DAARPR - Dallas)" ]
                            }
                      }
                    ]
              }
            , { slotTime = "2:20 PM"
              , slotType = "BREAKOUT SESSION"
              , sessions =
                    [ { title = "Fighting Side by Side with Families of the Victims of Police Crimes and Wrongful Conviction"
                      , sessionDescription = "A breakout session where families of victims and survivors of police crimes and wrongful convictions will share their stories of their experiences and how they joined the fight against these systems. We will facilitate discussion of how families are the fuel for our movement, and how we fight alongside them in both seeking justice for their loved ones, and in the struggle to end police crimes. (We will identify a few key families we’ve worked with to share their stories)."
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Just [ "Syd Loving (NAARPR Dallas)", "Jasmine Smith (CAARPR - Chicago)" ]
                            }
                      }
                    , { title = "Freeing All Political Prisoners"
                      , sessionDescription = "A presentation on freeing political prisoners and the fight to free those unjustly incarcerated."
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Just [ "Khaleed London (Jericho Movement)" ]
                            }
                      }
                    ]
              }
            , { slotTime = "3:45 PM"
              , slotType = "PLENARY"
              , sessions =
                    [ { title = "Executive Director’s Report"
                      , sessionDescription = """Adopt Action Items from breakout sessions and Introduction of resolutions for consideration. Present proposed slate for election of officers and board members and open the floor for nominations."""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "5:30 PM"
              , slotType = "RECESS"
              , sessions =
                    [ { title = "Doors close"
                      , sessionDescription = ""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "??? PM"
              , slotType = "NETWORKING"
              , sessions =
                    [ { title = "Social Gathering"
                      , sessionDescription = ""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            ]
      }
    , { day = "Sunday, November 5ᵗʰ"
      , slots =
            [ { slotTime = "8:00 AM"
              , slotType = ""
              , sessions =
                    [ { title = "Doors Open"
                      , sessionDescription = "Continental breakfast"
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "9:00 AM"
              , slotType = "PLENARY"
              , sessions =
                    [ { title = "Local Reports"
                      , sessionDescription = "Report-backs from Branches and Affiliate Organizations. Consideration and Voting on Resolutions"
                      , sessionLocation = "Main Hall"
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "11:00 PM"
              , slotType = "BREAK"
              , sessions =
                    [ { title = ""
                      , sessionDescription = ""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "11:15 PM"
              , slotType = "PLENARY"
              , sessions =
                    [ { title = "Resolutions"
                      , sessionDescription = "Consideration and voting of changes and ratifications of organizational bylaw amendments."
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "12:45 PM"
              , slotType = "BREAK"
              , sessions =
                    [ { title = "Lunch"
                      , sessionDescription = ""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "1:45 PM"
              , slotType = "PLENARY"
              , sessions =
                    [ { title = "Board Elections"
                      , sessionDescription = "Elections of Officers and National Board"
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "2:15 PM"
              , slotType = "BREAK/PRIVATE"
              , sessions =
                    [ { title = "Meeting of Newly Elected Board"
                      , sessionDescription = "Other attendees may socialize at the venue"
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "2:45 PM"
              , slotType = "RECESS"
              , sessions =
                    [ { title = "Conference Adjourns"
                      , sessionDescription = ""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            ]
      }
    ]
