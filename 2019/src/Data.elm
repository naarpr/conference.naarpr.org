module Data exposing (..)

---- HEADING ----


siteInfo : { title : String, tagline : String, homeLink : String }
siteInfo =
    { title = "NAARPR Conference"
    , tagline = "On November 22-24, we will gather in Chicago, Illinois to refound the National Alliance and bring the fight for #CommunityControlNow to cities across the country."
    , homeLink = "https://naarpr.org"
    }


geoLocation : { address : List String, mapSrc : String, mapUrl : String, geoUri : String }
geoLocation =
    { address = [ "CTU Hall", "1901 W. Carroll Avenue", "Chicago, IL 60612" ]
    , mapSrc = "https://www.openstreetmap.org/export/embed.html?bbox=-87.67702996730804%2C41.88651808028945%2C-87.6734894514084%2C41.88836311585891&amp;layer=mapnik&amp;marker=41.88744060473486%2C-87.67525970935822"
    , mapUrl = "https://www.openstreetmap.org/way/209829229"
    , geoUri = "geo:41.88744,-87.67559?z=19"
    }



---- MAIN ----


aboutInfo : { text : String, imgSrc : String, imgAlt : String }
aboutInfo =
    { text = "The Refounding Conference of the National Alliance Against Racist & Political Repression will take place in Chicago, Illinois on November 22-24. Activists and organizers from across the country will gather to refound NAARPR and coordinate nation-wide actions against state sponsored racial violence."
    , imgSrc = "/images/aboutBg.jpg"
    , imgAlt = "Angela Y. Davis, Frank Chapman, and Rasmea Odeh"
    }


quoteText : String
quoteText =
    "\"It is essential to resist the depiction of history as the work of heroic individuals in order for people today to recognize their potential agency as a part of an ever-expanding community of struggle.\" —Angela Y. Davis"


speakers : List { name : String, bio : String, imgSrc : Maybe (String) }
speakers =
    [ { name = "Angela Y. Davis, PhD"
      , bio = """Angela Davis is an American political activist, academic scholar, and author from Birmingham, Alabama.
She emerged as a prominent counterculture activist during the late 1960s while working with the U.S.
Communist Party and the Black Panther Party. In August 1970 at a Marin County California court room, Jonathan
Jackson and others took Judge Harold J. Haley, a district attorney, and several jury members hostage in an
attempt to publicize prison conditions and state abuses against the Soledad Brothers. This situation resulted in
the deaths of Judge Haley, Jonathan Jackson, James McClain, and William Christmas. Despite Davis not being in
the area at the time, the police named her an accomplice to the crime. 2 months later, she was captured and
imprisoned. Davis was acquitted of all charges on June 4, 1972. Upon release, she called for a national alliance to
free other political prisoners. This is what lead to the National Alliance Against Racist &amp; Political Repression
(NAARPR). In the 40+ years since her release, Davis has been a prolific writer on the themes of women’s rights
and leadership, prisoners’ rights, liberation politics, anti-racism, gender equity, and cultural studies. Some of
Davis’s published work includes: Women, Race &amp; Class, Are Prisons Obsolete? and Freedom is a Constant
Struggle: Ferguson, Palestine and the Foundations of a Movement."""
      , imgSrc = Just "/images/people/angeladavis.jpg"
      }
    , { name = "Frank Chapman"
      , bio = """Frank Chapman was wrongfully convicted of murder and armed robbery in 1961 and sentenced to life and 50
years in the Missouri State Prison. In 1973, the National Alliance Against Racist and Political Repression (NAARPR)
took up Frank’s case. In 1976 he was released after being incarcerated for 15 years. In 1983, he was elected
Executive Director of NAARPR. He worked alongside Charlene Mitchell, who preceded him as Executive Director
of NAARPR, and with Angela Davis on building an international campaign to free Rev. Ben Chavis and the
Wilmington Ten, Joann Little, Geronimo Pratt, Leonard Peltier and others falsely accused and politically
persecuted. Currently, Chapman is the Field Organizer and Educational Director of the Chicago Alliance Against
Racist &amp; Political Repression (CAARPR). He believes in the inalienable democratic right of Black people to
determine who polices their communities and how their communities are policed. Chapman recently published
his autobiography The Damned Don’t Cry: Pages from the Life of a Black Prisoner &amp; Organizer."""
      , imgSrc = Just "/images/people/frankchapman.jpg"
      }
    , { name = "Ariel Atkins"
      , bio = """Ariel Atkins is the lead organizer of Black Lives Matter Chicago. She has been a member of BLMChi for three
years and a member of the alliance for four. She was heavily involved in the #Justice4Laquan campaign, is
currently involved in the Justice for Ronnieman campaign, the campaign for CPAC, and many others throughout
Chicago. She believes that nothing can be won without educating and mobilizing the people for our collective
win. She's a growing abolitionist, likes coconut oil in her hair and white fragility tears in her coffee."""
      , imgSrc = Just "/images/people/arielatkins.jpg"
      }
    , { name = "Betty Davis"
      , bio = """I have been a warrior in the struggle for community control of education every since the NYC school
strike in 1967. .I was a social worker and an educator. I helped form The New Abolitionist Movement because as
everyone knows, &quot;Cats don&#39;t educate mice, they eat them.&quot; This organization grew out of the movement to
support the Ocean Hill Brownsville Community Control struggle and the rest is history. I am dedicated to the
institutions of the public school system because to paraphrase Frederick Douglass, ‘education and slavery are
incompatible’. As a member of the Lynne Stewart Org., I was proud to serve as financial administrator and
fundraiser.
The entire movement of the sixties was based on the belief that our children are the future and it is the duty of
each generation to guarantee the future of the ones who come after them.
I joined the Green Party decades ago after college but only became active when Cynthia McKinney ran for the
presidency. I may still be addicted to potato chips and chocolate but I am not addicted to a 2 party mythology or
the dixiecrat Democrats."""
      , imgSrc = Just "/images/people/bettydavis.jpg"
      }
    , { name = "Bernadette Ellorin"
      , bio = """Bernadette Ellorin serves as the Vice-Chairperson for North America for the International League of Peoples
Struggles or ILPS, an anti-imperialist and democratic formation representing national and social liberations
movements present in over 45 countries and autonomous states and in 6 continents. Ellorin is also the national
spokesperson and former chairperson of BAYAN USA, an alliance of over 20 Filipino grassroots organizations in
the US representing workers, women, youth and students. It is the oldest and largest overseas chapter of BAYAN
Philippines, the main political center of the national democratic movement with a socialist perspective in the
Philippines."""
      , imgSrc = Just "/images/people/bernadetteellorin.jpg"
      }
    , { name = "Toshira Garraway"
      , bio = """Toshira Garraway is the fiance of Justin Tiegen, and mother to his young son. Justin was killed in 2009, when by
St. Paul police who beat him to death and dumped his body in the garbage dumpster."""
      , imgSrc = Just "/images/people/toshiragarraway.jpg"
      }
    , { name = "Cherrrene Horazuk"
      , bio = """Cherrene Horazuk has been an organizer for over 30 years. She is the president of AFSCME 3800 and the
secretary of AFSCME Council 5 in Minnesota. In her 15 years of union activism, she has led a strike, and helped
win a $15 minimum wage, paid parental leave, and workplace protections for trans workers. Prior to union
organizing, Cherrene was the director of CISPES and led campaigns against US intervention and in support of the
people’s movements. She helped lead a 100,000 person antiwar rally during the second gulf war, was on the
national steering committee for Stonewall 25, participated in an ACT-UP protest that shut down every bridge and
tunnel in Manhattan during rush hour, and participated in a 100+ person civil disobedience at the Liberty Bell to
demand freedom for Mumia Abu Jamal."""
      , imgSrc = Just "/images/people/cherrenehorazuk.jpg"
      }
    , { name = "Bassem Kawar"
      , bio = """Bassem Kawar is a Chicago-based Jordanian American who graduated with a Bachelor of Arts in Sociology from
DePaul University, where he was active with Students for Justice in Palestine (SJP). He began his career as a
community organizer with the Arab American Action Network (AAAN), working predominantly in the Arab
American and Arab immigrant communities of Chicago and its southwest suburbs. Over the past few years,
Kawar worked on Jesus “Chuy” Garcia’s historic Chicago mayoral campaign, organized with the teachers in
Detroit on their rank-and-file contract battle with the city, and led the National Campaign to TAKE ON HATE. He
is a leader in Chicago of the U.S. Palestinian Community Network (USPCN), passionate about supporting the
uplifting of oppressed communities, and believes that grassroots organizing and strategic alliances are the best
ways to achieve that."""
      , imgSrc = Just "/images/people/bassemkawar.jpg"
      }
    , { name = "Christina Kittle"
      , bio = """Christina Kittle is a rank and file educator and community organizer from Jacksonville, Florida. She works with
the Jacksonville Community Action Committee and Coalition for Consent. After being a political target of the
police, which led to a peaceful protest turning into a police brutality case where her and four other activists were
put on trial and won their freedom, she now works with her organizations to fight for police accountability. The
demand is for a police accountability council wherein which community members are democratically elected on
a review board with the power to fire corrupt police found guilty of crimes against the people. She has also been
active in organizing for justice for Black transgender women killed in Jacksonville when the police refused to
adhere to the federal practices of Matthew Shepard and James Byrd Jr law, which dictates that police should use
perceived gender identity during the investigation of hate crimes. She continues to push for police accountability
and gender liberation in the Deep South."""
      , imgSrc = Just "/images/people/christinakittle.jpg"
      }
    , { name = "Sydney Lovіng"
      , bio = """Sydney Lovіng is co-chair of the North Texas Action Committee, a Dallas-based organization dedicated to fighting
police brutality, prison profiteering, racist political repression, and economic injustice. She has been an activist
since she was a student organizing with Decolonize Media Collective, which led the campaign to force the
university&#39;s divestment from the private prison industry, and organized with BLM 413 and Out Now in
Springfield, MA (2013-2017). She has worked as a kindergarten teacher for the past year and a half."""
      , imgSrc = Just "/images/people/sydneylovіng.jpg"
      }
    , { name = "Pastor Emma Lozano"
      , bio = """Pastor Emma Lozano has been an activist for the immigrant/undocumented community for decades. She was
born in Texas and raised in the Pilsen neighborhood of Chicago. As a teenager and young adult, she worked
alongside her brother Rudy Lozano, an activist for workers and the Latino community who was assassinated in
1983. Since his death, Lozano has dedicated her life to the continuation of her brother’s work.
She is the founder and President of Centro Sin Fronteras where she has worked to stop thousands of
deportations. In the 1990s and early 2000s she was active in the struggle for undocumented parents to get
control of their children’s schools through local school councils, has fought against gentrification, and was a part
of organizing the largest immigrant rights marches in 2006.
She is a co-founder of Familia Latina Unida along with Elvira Arellano who she was pastor to when she stayed in
sanctuary in 2006 at Adalberto UMC. She is currently the pastor of sanctuary church Lincoln UMC where she
continues to counsel those who are in deportation proceedings and works with the local youth to close the 20
year life expectancy gap that exists in Chicago.
She is a two time breast cancer survivor and works with the Youth Health Service Corps to provide free
mammograms to the undocumented and uninsured communities in Chicago through RUSH University. This
program runs in over 20 CPS grammar and high schools and works with dozens of health clinics and hospitals in
the city of Chicago. Lincoln UMC is a sanctuary to the undocumented and LGBTQ communities.
Pastor Lozano was recently appointed as the Chaplain to the National President of LULAC and has been President
of Council 5294 since 2014. She is also the Chair for Womens Issues at the LULAC Illinois level."""
      , imgSrc = Just "/images/people/emmalozano.png"
      }
    , { name = "Miriam Magaña"
      , bio = """Miriam Magaña organizes with the Minnesota Immigrant Rights Action Committee (MIRAC)"""
      , imgSrc = Just "/images/people/miriammagaña.jpg"
      }
    , { name = "Kent Marrero"
      , bio = """Kent Marrero is a Queer, Puerto Rican community advocate who has worked alongside grassroots liberation
movements across the Panhandle, North Florida, and Central Florida for over a decade. They currently serve as
the Jacksonville Advocacy Coordinator for QLatinx and they are an Equality Florida&#39;s Jacksonville TransAction
Council Member. Previously, Kent provided LGBTQ+ representation serving as a staff writer for the Deep South’s
largest LGBTQ magazine covering stories about hate crime cases and interviewing comedians like Devin Green.
Kent sprang to action after the Pulse nightclub attack. First hand experience navigating reporters at The LGBT
Center of Orlando reinforced that framing is essential to keeping control of community narratives. The media
reported Pulse as a terrorist attack until Orlando LGBTQ+ advocates pushed to reclaim the narrative as an
LGBTQ+ hate crime. Six months after Pulse, Story Core hosted a Taking Your Pulse forum where the community
began to pose questions surrounding racial erasure within the public Pulse narrative. This forum is also where
Kent called on the media to intentionally reflect on how they can cover stories with more respect and empathy
to the family, survivors, and affected community in the audience."""
      , imgSrc = Just "/images/people/kentmarrero.jpg"
      }
    , { name = "CeCe McDonald"
      , bio = """CeCe McDonald a Minneapolis-based artist and activist. As a Black transgender woman, she is a survivor of
transphobic violence, criminalization, and incarceration. During her time in prison, her writing inspired an
international community of activists to support the #FreeCeCe campaign and to advance the broader
movements for transgender liberation and prison abolition. Since her release in 2014, she has become a
frequent speaker and commentator on these issues. She has worked with numerous organizations, including the
Transgender Youth Support Network in Minneapolis and the Gender Justice League in Seattle; and was selected
as a 2018 Activist-in-Residence at the Barnard Center for Research on Women and one of this year’s Soros
Justice fellows."""
      , imgSrc = Just "/images/people/cecemcdonald.jpg"
      }
    , { name = "Tracy Molm"
      , bio = """Tracy Molm is a member of the Minnesota Anti War Committee and Freedom Road Socialist Organization. In
2010 her apartment was one seven homes raided by the FBI as part of an investigation of solidarity and anti war
activists. Tracy was also one of the 23 activists that refused to testify to a grand jury as part of this same
investigation."""
      , imgSrc = Just "/images/people/tracymolm.jpg"
      }
    , { name = "Carlos Montes"
      , bio = """Carlos Montes is a nationally recognized immigrant rights, antiwar, pro public education Chicano activist and
former political prisoner. His family immigrated from Mexico and he grew up in Los Angeles. Montes was part of
Chicano civil rights movement and co-founder of the Brown Berets, active in MEChA and first Chicano
Moratorium against Vietnam war 1969. He participated in the Poor Peoples Campaign in 1968, Latinos for
Jackson in 1984, and the march on the RNC in 2008 and 2012.
Montes was arrested many times for his organizing. Also indicted twice by a grand jury, 1968 he was part of the
East LA 13 who were blamed for the High School Walkouts, as part of the Biltmore 6 for their protest on Gov.
Reagan, and most recently in the aftermath of the 2010 Grand Jury subpoenas and raids on antiwar and
international solidarity activists. Due to COINTELPRO the many arrests and threats to life he was forced to go on
the run in the early 1970s. He lived in Mexico and later in El Paso, Tejas where he organized support for the
Farah textile strike and the La Raza Unida Party.
Currently retired and a community organizer with Centro CSO in Boyle Heights neighborhood of Los Angeles, and
a writer for Fight Back! News. In Boyle Heights he has also been an advocate for the environment working to
close Exide battery cycling plant. Has been a longtime advocate for public education and organized for new high
schools and to oppose charter takeovers of public schools."""
      , imgSrc = Just "/images/people/carlosmontes.jpg"
      }
    , { name = "Sheridan Murphy"
      , bio = """Sheridan Murphy is President of the Board of the International Leonard Peltier Defense Committee and served
as the State Executive Director of the American Indian Movement of Florida from 1987-2006. Sheridan has been
active in Indian rights since joining the Tiospaye Indian Student Council at the University of South Dakota in 1983.
With a group of concerned Indian and non-Indian students he helped found the Tiyospaye Indian Student
Organization at the University of South Florida in 1985. With David Goyette he first founded the Florida Indian
Alliance in 1985 and then the Florida AIM chapter in1987. He served as executive director of Florida AIM from its
founding in 1987 until 2007. He joined the board of the International Leonard Peltier Defense Committee in 2017
and became the President of the Board in 2018.
Sheridan first came to action in Florida in 1986 when he lead the effort to close the display of five Timucuan
peoples remains at the Crystal River State Archaeological site . He was active in virtually every Florida AIM effort
over twenty years ranging from the 1989-93 campaign to change the name of the Hernando De Soto festival to
efforts to force FSU to remove its stereotypical mascot to the successful 1995 campaign to defend the
Independent Traditional Seminole Nation from relocation.
Sheridan has been active in community efforts being deeply involved in the multicultural CommUNITY campaign
in the Tampa Bay area serving in the 1990s on the Pinellas County School Board&#39;s Multicultural advisory board.
He has been recognized for his service by Brevard Community College, Leonard Peltier Defense Committee, Bring
Peltier Home Campaign, and the National Peoples Democratic Uhuru Movement among others. He was
nominated as a delegate to the II and III Continental Encounters in Guatemala City, Guatemala and Esteli,
Nicaragua respectively and was a 1995 nominee for the ACLU Medal of Liberty."""
      , imgSrc = Just "/images/people/sheridanmurphy.jpg"
      }
    , { name = "Ralph Poynter"
      , bio = """Ralph Poynter was born in Western Pennsylvania, the son of a union organizer when Pittsburgh and surrounding
area was the steel capital of the world. I remained true to the principles of unionism as founding member of the
Teacher’s Freedom Party Caucus of the UFT. I continued supporting true union principles when the UFT joined
management by supporting the racist Central Board of Education in NYC against the community control of
schools movement.
After being released from jail, caused by my forcibly installing the first Black and Latino principals into the NYC
school system, I continued the struggle for justice as a Private Investigator who confronted injustice directly. I
successfully supported Lynne Stewart in her lifelong struggle against the double standard so often practiced in
the U.S. injustice system pasts and present by launching a victorious opposition to her incarceration and bringing
her home."""
      , imgSrc = Just "/images/people/ralphpoynter.png"
      }
    , { name = "Ashley Quiñones"
      , bio = """Ashley Quiñones is the wife of Brian Quinones, who was murdered by 5 officers in Richfield, Minnesota in
September 2019. Brian was a father and a hip hop artist named Blessed The MC."""
      , imgSrc = Just "/images/people/ashleyquiñones.jpg"
      }
    , { name = "Regina Russell"
      , bio = """Regina Russell: My son Tamon Russell was Wrongly Convicted in 2001 and was given a 45-year sentence with no
physical evidence and two unreliable witnesses, even though he was at home with his younger sister and myself
at the time of the alleged crime. He was brutalized and denied the right to see an attorney. The senior officer
who signed the charges against Tamon, Raymond Madigan, was one of the Jon Burge gang of cops. I have always
been a fighter as a union steward in SEIU Local 73 and an Officer, and it was my Union brother and friend, Joe
Iosbaker, who got me involved in the Chicago Alliance and its Campaign To Free Incarcerated Survivors Of Police
Torture (CFIST). Now together we fight we fight for justice for victims of torture and the wrongly convicted."""
      , imgSrc = Just "/images/people/reginarussell.jpg"
      }
    , { name = "Michael Sampson"
      , bio = """Michael Sampson started as an activist on the campus of Florida State University where he co-founded multiple
organizations including Progress Coalition and the Dream Defenders. He has organized on issues regarding
education rights as well as combating police brutality and building a national movement for community control
of the police.
With the Dream Defenders, he was a part of the movement to demand justice for Trayvon Martin and ending the
school to prison pipeline in Florida. He covered the Ferguson rebellions following the murder of Mike Brown as
well as the Baltimore rebellion following the murder of Freddie Gray by police for Fight Back News.
Michael has organized in Tallahassee and DC, and now organizes in his hometown of Jacksonville, Florida where
he helped start the Jacksonville Community Action Committee( JCAC) in 2017 where he has organized for
community control of the police."""
      , imgSrc = Just "/images/people/michaelsampson.jpg"
      }
    , { name = "Nazek Sankari"
      , bio = """Nazek Sankari has been a member of the US Palestinian Community Network (USPCN) for two years where she
serves on the national membership committee for the Chicago chapter. She is currently pursuing a master&#39;s
degree in Social Work with an interdepartmental concentration in Gender and Women&#39;s Studies."""
      , imgSrc = Just "/images/people/nazeksankari.jpg"
      }
    , { name = "Jess Sundin"
      , bio = """Jess Sundin was a founding member of the Anti-War Committee in Minneapolis, organizing against US wars in
Iraq and Afghanistan, and military aid to the repressive governments of Colombia and Israel. In 2010, an FBI
investigation into this activism led to an FBI raid on her home (and several others), and she was one of 23 people
ordered to testify before a federal grand jury about their international solidarity work. As a grand jury resister,
Jess helped to build the Committee Stop FBI Repression. In the midst of that work, the November 2015 police
murder of Jamar Clark brought forth a powerful movement in Minneapolis to demand #Justice4Jamar. Jess
helped found the Twin Cities Coalition for Justice 4 Jamar, where she continues the work with the families of
Jamar and others to fight against police crimes for community control of police."""
      , imgSrc = Just "/images/people/jesssundin.jpg"
      }
    , { name = "Masao Suzuki"
      , bio = """Masao Suzuki has been active in the Japanese American community in the San Jose, CA for more than 30 years
with the Nihonmachi Outreach Committee (NOC). In February of 2017 he gave the keynote speech as the annual
Day of Remembrance event which commemorates the World War II concentration camps for Japanese
Americans. Mr. Suzuki was also a lead organizer for a Japanese American - American Muslim solidarity march and
rally in San Jose in March of 2017.
In 2010 Masao Suzuki was visited by the FBI as part of the raids and Federal Grand Jury subpoenas aimed at
antiwar and international solidarity activists in the Midwest. He is a frequent writer for Fight Back! newspaper
and is a member of the Freedom Road Socialist Organization (FRSO). He has taught economics at a community
college for the last 20 years."""
      , imgSrc = Just "/images/people/masaosuzuki.png"
      }
    , { name = "Loretta VanPelt"
      , bio = """Loretta VanPelt is a founding member and organizer with the Twin Cities Coalition for Justice 4 Jamar in
Minneapolis, Minnesota."""
      , imgSrc = Just "/images/people/lorettavanpelt.jpg"
      }
    , { name = "Tanya Watkins"
      , bio = """Tanya Watkins is a mother, writer, Black womanist, community organizer and prison abolitionist originally from
Chicago’s South Side. She studied writing and the teaching of writing at Columbia College Chicago with the intent
of bringing innovative arts programs to schools in low-income communities of color. She is a former South
Suburban Chicago elected official and served as a Presidential Delegate in 2016. Tanya began as a leader with
Southisiders Organized for Unity and Liberation (SOUL) in 2010, a multi-issue, faith-based, social justice
organization that assists residents to build power in the Chicago Southland. Since being with the organization,
Tanya has led a multitude of campaigns demanding criminal justice reform and police accountability in Chicago;
most notably, heading up a series of strategic actions targeting former Cook County State’s Attorney, Anita
Alvarez in 2015. Currently, Tanya is the Executive Director of SOUL and SOUL in Action."""
      , imgSrc = Just "/images/people/tanyawatkins.jpg"
      }
    , { name = "Brian L. Ragsdale, PhD"
      , bio = """Brian L. Ragsdale, is a senior core faculty and skills development coordinator for the School of
Psychology programs at Walden University. He is an accomplished mixed media artist, self-taught songwriter/pianist, and
lives on Chicago’s southside. He currently serves as President of the Association of Black Psychologists: Chicago chapter
(2018-2020) and on the executive board of the Chicago Alliance Against Racist and Political Repression. He earned his MA
(1998) and PhD (2000) in Clinical Psychology from the University of Rhode Island and is a licensed clinical psychologist in
Illinois (2001-2018). Their teaching, counseling, and community organizing work is focused on addressing carceral systems
including police brutality, and promoting collective healing responses to anti-Black racism, stress, and trauma. They have
published and presented on a range of topics from exploring multidimensional identity concerns, adolescent development,
LGBTQI, digital learning, social media, and spirituality."""
      , imgSrc = Nothing -- Just "/images/people/brianragsdale.jpg"
      }
    , { name = "Devin Branch"
      , bio = """Devin Branch is a political activist and advocate for self-determination for oppressed nations. He is a former
member of the National Steering Committees of the Jericho Movement and is currently a contributor to Re-
Build: A New Afrikan Nationalist Periodical."""
      , imgSrc = Nothing -- Just "/images/people/devinbrranch.jpg"
      }
    , { name = "Edwin Cortes"
      , bio = """Edwin Cortes was a student activist at the University of Illinois at Chicago when he was arrested in the early
1980s. He became one of 10 men and five women Puerto Rican political prisoners in the U.S. They were targeted
for their work advocating for independence for their homeland. They were given decades long sentences, even
though no one was injured, nor was there any property damage underlying the charges brought against them by
the U.S. government. The movement to obtain their freedom mushroomed into a broad-based human rights
campaign stretching from Puerto Rico to Chicago. Pardoned in 1999, Cortes continued to campaign for freedom
for Oscar Lopez Rivera, who was finally released in 2017."""
      , imgSrc = Nothing -- Just "/images/people/edwincortes.jpg"
      }
    , { name = "Stacy Davis Gates"
      , bio = """Stacy Davis Gates is the Vice President of Chicago Teachers Union (CTU), Local 1. She was an important leader in
the path breaking strike that CTU lead in Chicago this fall, and has been a force for changing Chicago politics from
the ground up. She has served as the Political and Legislative Director for the Chicago Teachers Union for the
past six years. While at the CTU, Ms. Davis Gates has been the architect of bold political and legislative
campaigns for the schools and city that all Chicagoans deserve. She has raised millions of dollars to elect
classroom teachers to all levels of local government and to challenge school privatizers and union-busters, most
prominently Mayor Rahm Emanuel and his rubber-stamp City Council in 2015. Ms. Davis Gates has also
spearheaded successful statewide legislative campaigns to pass the strongest charter school accountability
measures in the country, to restore the bargaining rights of Chicago Public Schools employees, and to fully fund
public education by ending tax loopholes for the 1%. In 2017, Ms. Davis Gates was elected Chair of United
Working Families, an independent political organization by and for working class people and our movements.
She also serves as a board member for ACRE, The Action Center on Race &amp; the Economy, a nexus for
organizations working at the intersection of racial justice and Wall Street accountability. Ms. Davis Gates is
currently on leave from the classroom, where she taught high school social studies for over a decade at
Englewood and Clemente High Schools and Mason Community Links High School. She attended Saint Mary’s
College, the University of Notre Dame, and Concordia University. Ms. Davis Gates lives on the south side of
Chicago with her husband and three children."""
      , imgSrc = Nothing -- Just "/images/people/stacydavisgates.jpg"
      }
    , { name = "Masai Ehehosi"
      , bio = """Masai Ehehosi is a citizen of the Republic of New Afrika, and a long time activist in the New Afrikan
Independence Movement (NAIM). He is a former Black Liberation Army POW. Masai is a board member of
Critical Resistance (CR), a grassroots organization that works to build a mass movement to abolish the Prison
Industrial Complex (PIC). He currently works with the National Jericho Amnesty Movement for Political Prisoners
and Prisoners of War, and the Imam Jamil Al-Amin (f/k/a H.Rap Brown) Action Network. Masai is a Muslim, and
part of Masjid Al-Kabah."""
      , imgSrc = Nothing -- Just "/images/people/masaiehehosi.jpg"
      }
    , { name = "Bertha Escamilla"
      , bio = """My name is Bertha Escamilla. I became active in the movement because my son Nick Escamilla was kidnapped
and arrested by corrupt detectives, Kenneth Boudreau, John Halloran and James O’Brien, who have made
careers of locking up innocent Latino and Black men and women. My son Nick was arrested in February 1993 for
a murder he knew nothing about nor had any involvement in. The detectives forced Nick to make a statement by
beating, torture and threatening to arrest his pregnant wife and take their 2-year old daughter to DCFS. Nick
served 14 1/2 years for murder. Nick served this time and yet is still threatened as a felon. I have been active in
trying to get justice for him and so many others. We must not stop. We need to fight to get justice for all. We
have to hold corrupt officers, judges and prosecutors accountable for their actions. Laws need to change. We can
do this if all fight for justice. We demand freedom for torture victims!"""
      , imgSrc = Nothing -- Just "/images/people/berthaescamilla.jpg"
      }
    , { name = "Jessie Fuentes"
      , bio = """Jessie Fuentes is an activist in the Humboldt Park Community in Chicago, IL. Fuentes is a graduate of Dr. Pedro
Albizu Campos Puerto Rican High School – the Puerto Rican Cultural Center’s very own educational institution.
Fuentes quickly after went to pursue her Bachelor’s Degree in Justice Studies and Latino and Latin American
Studies (double majored) and has recently earned her Graduate Degree in Community and Teacher Leadership
(MA). Fuentes has done work around empowering young people in the community, the release of Oscar Lopez
Rivera, anti-gentrification work, education reform, and work around the independence of Puerto Rico. Fuentes
began her political work as the Director of a youth organization in Humboldt Park called the Batey Urbano.
Fuentes is currently the Dean of Students at Dr. Pedro Albizu Campos Puerto Rican High School. Fuentes is also
the Co-Chair of the Puerto Rican Agenda in Chicago. The Puerto Rican Agenda is an unincorporated organization
of local Puerto Rican leaders that seek to influence policy for the advancement of the Puerto Rican community in
Chicago."""
      , imgSrc = Nothing -- Just "/images/people/jessiefuentes.jpg"
      }
    , { name = "Kimberly Handy-Jones"
      , bio = """Kimberly Handy-Jones of Waukegan, IL, is the mother of Cordale Q Handy, who was killed by St. Paul police in
March 2017. Ms. Kim regularly travels to Minnesota, to fight for justice for her son and others killed by police in
Minnesota and beyond. She founded the Cordale Q. Handy In Remembrance of Me Foundation (
https://www.cordaleqhandyinremembranceofme.org/ ), and hosts an annual banquet to honor mothers of
stolen lives and provide burial cost support."""
      , imgSrc = Nothing -- Just "/images/people/kimberlyhandy-jones.jpg"
      }
    , { name = "Esther Hernandez"
      , bio = """Esther Hernandez was nine years old when she moved to Chicago from Puerto Rico with her family. She raised
three children in Chicago and has four beloved grandchildren. Twenty-two years ago, Esther’s sons, Juan and
Rosendo Hernandez became victims of Chicago Police Detective Reynaldo Guevara’s coercive police practices. In
1998, Esther joined Comite Exigimos Justicia in an effort to raise awareness about Guevara and his victims. A
group of resilient women who organized and started a movement to expose corrupt officers Reynaldo Guevara,
Ernest Halvorsen and others who targeted their community. In 2013, Innocent Demand Justice Committee was
born. Esther and her son Juan Hernandez decided to start their own committee since the previous group had
faded away. Innocent Demand Justice Committee holds monthly meetings, are present at court cases, has
motivational speakers at their meetings, holds group and family discussions and collaborates with other
organizations to improve and strengthen families all in the names of the Wrongfully Convicted."""
      , imgSrc = Nothing -- Just "/images/people/estherhernandez.jpg"
      }
    , { name = "Dorothy Holmes"
      , bio = """Dorothy Holmes is the mother of Ronald RonnieMan Johnson who was murdered on October 12th 2014 by the
Chicago Police Department. My fight continues. I will never give up the fight for justice for my son."""
      , imgSrc = Nothing -- Just "/images/people/dorothyholmes.jpg"
      }
    , { name = "LeTanya Jenifor-Sublett"
      , bio = """LaTanya Jenifor-Sublett experienced police abuse and torture at Chicago Area 2 police station at the age of 19.
Sentenced to 42 years in the Illinois Department of Corrections at the age of 21 and subsequently spent 21 years
in prison for a crime that she did not commit. During her incarceration she spent her time studying law, business,
and social justice. Released from prison October 4, 2013, LaTanya has become an active member of several
Chicago social justice organizations advocating for human rights, social justice, and prison reform and sentencing
reform. As a member of the Chicago Torture Justice Center Speaker’s Bureau, LaTanya spends time speaking to
students at Chicago Public Schools about the history and trauma of police violence. She is also a member of the
Survivor and Family Advisory Council for the Chicago Torture Justice Center and the Harm Reduction “All of Us or
None” Advisory board for Men and Women in Prison Ministries (MWIP)."""
      , imgSrc = Nothing -- Just "/images/people/letanyajenifor-sublett.jpg"
      }
    , { name = "Marisol Marquez"
      , bio = """Marisol Marquez is a working-class Chicana, a proud member of Freedom Road Socialist Organization and has
been fighting against police brutality for years. Marquez grew up as a migrant farmworker and has lived in both
the East and West Coast.
When she lived in Florida, a racist vigilante killed 17-year-old Trayvon Martin. Marquez helped lead protests
outside the courthouse, as the trial took place. She saw how Zimmerman taunted protesters from the
courtroom. After Zimmerman was found not guilty of manslaughter, Marquez joined many in calling for national
protests.
Now living in East LA she continues to fight for Chicanos, Latinos, Central Americans. Yearly the organization she
is also a part of Centro Community Service Organization, puts on a Chicano Moratorium commemoration. These
events center around the 1970 moratorium against the Vietnam War. Currently CSO connects the 1970 demands
with demands of today which include taking community control over LAPD/LASD."""
      , imgSrc = Nothing -- Just "/images/people/marisolmarquez.jpg"
      }
    , { name = "Melissa McKinnies"
      , bio = """Melissa McKinnies is an outspoken activist of the Ferguson uprising of 2014 and the mother of Danye said on
Nov.2, 2019, &quot;They lynched my baby.&quot; The police tried to call it suicide but sister Melissa Jones said: &quot;The knots
in the bed sheets, which did not come from my home, were Navy knots. My son was not in any military, not a
boy scout.&quot; Her son Danye Jones was hung on October 17, 2018. A year later to the date Javon Jones, 23 year old
brother of Danye Jones, was arrested in Chicago and falsely charged, along with his friend Robert Wilkes, with
use of a fire arm. Both brother Jones and Wilkes had permits. Melissa Jones and Rayna Turner, the mother of
Robert Wilkes, came to the Chicago Alliance office seeking help. In the wake of organized protests led by Black
Live Matter Chicago and the Chicago Alliance Against Racist and Political Repression Javon Jones and his friend
Robert Wilkes were both released, after charges were dropped, on Nov. 8, 2019."""
      , imgSrc = Nothing -- Just "/images/people/melissamckinnies.jpg"
      }
    , { name = "Rasmea Odeh"
      , bio = """Rasmea Odeh was born in Lifta, near Jerusalem, Palestine, in 1947, and was forced from her home a year later
during the Nakba (Arabic for &quot;Catastrophe&quot;), which describes the exile of 750,000 Palestinians upon the founding
of the colonialist state of Israel in 1948.
Until 1994, when she immigrated to the U.S., Odeh worked in Jordan, Lebanon, and Palestine at different times
in refugee resettlement, as a women&#39;s rights organizer, and as an attorney. She was a leading organizer in
Chicagoland’s Palestinian and Arab communities from 2004, when she became the Associate Director of the Arab
American Action Network (AAAN), until 2017, co-founding the AAAN’s Arab Women’s Committee, which has
over 800 members and leads the organization’s work fighting against racial profiling and defending immigrant
rights.
In 1969-70, she was tortured and sexually assaulted into a false “confession” by the Israeli military, and served
10 years in prison, becoming one of the most famous Palestinian political prisoners in the world. For allegedly
not disclosing this forty-five year old “conviction” by an Israeli military court, Rasmea was indicted in 2013 by the
U.S. Department of Justice on an immigration violation, nine years after she had already gained U.S. citizenship.
After three and a half years of valiantly battling for her freedom, and knowing that a Palestinian cannot get a fair
trial in U.S. courts, Rasmea accepted a plea agreement, and was forced to permanently leave the States in
September of 2017. Her cause garnered massive attention and support from a number of the most important
social justice movements in the U.S., including the Black Liberation Movement, as her life story embodies the
brave Palestinian people’s struggle for self-determination, the Right of Return, and independence."""
      , imgSrc = Nothing -- Just "/images/people/rasmeaodeh.jpg"
      }
    , { name = "Aislinn Pulley"
      , bio = """Aislinn Pulley is a co-executive director of the Chicago Torture Justice Center founded out of the historic 2015
reparations ordinance for survivors of Chicago police torture. Aislinn is also a lead organizer with Black Lives
Matter Chicago. She was an organizer with We Charge Genocide, a founding member of Insight Arts, a cultural
non-profit that used art for social change, and a member of the performance ensemble, End of the Ladder. She is
a founder of the young women’s performance ensemble dedicated to ending sexual assault, Visibility Now, as
well as the founder and creator of urban youth magazine, Underground Philosophy."""
      , imgSrc = Nothing -- Just "/images/people/aislinnpulley.jpg"
      }
    , { name = "Rhonda Ramiro"
      , bio = """Rhonda Ramiro has been an organizer in the movement for National Democracy in the Philippines since the
1990s. Rhonda was a founder of BAYAN-USA in 2005, establishing the first overseas chapter of the Philippines-
based Bagong Alyansang Makabayan (“New Patriotic Alliance” or BAYAN for short), a nationwide, multi-sectoral
alliance of over a thousand grassroots peoples’ organizations in the Philippines, fighting for national and social
liberation. Rhonda is currently the Chair of BAYAN-USA, a member of the International Coordinating Committee
of the International League of Peoples&#39; Struggle, and a member of the Administrative Committee of the United
National Anti-War Coalition."""
      , imgSrc = Nothing -- Just "/images/people/rhondaramiro.jpg"
      }
    , { name = "Jazmine Salas"
      , bio = """Jazmine Salas is a Puerto Rican and Dominican organizer living in Chicago. She grew up in the Puerto Rican
diaspora, in Orlando, FL, and became involved in the movement to stop police crimes in 2015 after moving to
Chicago to attend grad school. She is the current co-chair of the Chicago Alliance Against Racist and Political
Repression and the Chicago Boricua Resistance."""
      , imgSrc = Nothing -- Just "/images/people/jazminesalas.jpg"
      }
    , { name = "Monique Sampson"
      , bio = """Monique Sampson is an activist with UNF Students for a Democratic Society. In 2017, Monique led the student
movement against a self proclaimed Neo-Nazi who threatened UNF SDS. UNF SDS was able to successfully get
the Nazi kicked off campus as well as win many demands for black students such as: increased black enrollment,
increase in scholarships and an increase in black faculty and staff. Since then, Monique has fought for a
sanctuary campus for undocumented students, mental health services for black students, and community
control of the police. My favorite thing to do to unwind after an action is to watch Christmas movies with my
partner, dog and cat."""
      , imgSrc = Nothing -- Just "/images/people/moniquesampson.jpg"
      }
    , { name = "Muhammad Sankari"
      , bio = """Muhammad Sankari is a lead organizer with the Arab American Action Network, and helps guide the AAAN&#39;s
Youth-lead campaign to end racial profiling."""
      , imgSrc = Nothing -- Just "/images/people/muhammaddsankari.jpg"
      }
    , { name = "hondo t'chikwa"
      , bio = """hondo t’chikwa is a long time activist in the New Afrikan Independence Movement (NAIM). He has worked for
the release of New Afrikan political prisoners &amp; prisoners of war in particular, &amp; all political prisoners in general,
for three decades. he works with the Re-Build Collective &amp; the National Jericho Amnesty Movement. he can be
reached at: info@rebuildcollective.org."""
      , imgSrc = Nothing -- Just "/images/people/hondot'chikwa.jpg"
      }
    , { name = "Mildred Williamson, PhD, MSW"
      , bio = """Mildred Williamson, has spent her teenage and adult life engaged in the work of social justice
movements. Most notably, she was an early member of the original National Alliance Against Racist &amp;amp;
Political Repression, co-led the Chicago Chapter and served as a national board member for many years. Her
career in public service is also framed by human rights/social justice. She has more than 25 years of experience in
developing and leading public health safety net programs for vulnerable populations. She has held a number of
positions over the years within the Cook County Health (CCH) system. She was the first administrator of the
Women &amp;amp; Children HIV Program, which today, is part of the Ruth M. Rothstein CORE Center, where she
currently serves as Executive Director. She is also an adjunct assistant professor at the University of Illinois at
Chicago School of Public Health (UIC-SPH)."""
      , imgSrc = Nothing -- Just "/images/people/mildredwilliamson.jpg"
      }
    , { name = "Arewa Karen Winters"
      , bio = """Arewa Karen Winters was born and raised on Chicago’s Greater Westside. She is a freedom fighter, activist and
serves on the frontline for justice around police violence and terrorism. Her organizational affiliations include
The 411 Movement for Pierre Loury, Justice for Families, a chapter of Black Lives Matter Chicago and Women’s
All Points Bulletin. She is also a spoken word artist and classically trained chef who feels as though it is her duty
to feed and liberate her people."""
      , imgSrc = Nothing -- Just "/images/people/arewakarenwinters.jpg"
      }
    , { name = "Danya Zituni"
      , bio = """Danya Zituni is a member of US Palestinian Community Network, a national Arab and Palestinian grassroots
organization."""
      , imgSrc = Nothing -- Just "/images/people/danyazituni.jpg"
      }
    , { name = "Danya Zituni"
      , bio = """Christel Williams-Hayes is a 25-year veteran of Chicago Public Schools, serving as a PSRP delegate, district
      supervisor, functional vice president and American Federation of Teachers-Illinois Federation of Teachers (AFT-IFT)
      convention delegate in that time. Christel was a Chicago Teachers Union organizer during the 2012 strike, as well as the
      three-day march in 2013 and CTU Summer Organizing Institute prior to becoming a Union field representative in 2017. She
      is an IFT executive board vice president, member of the IFT Scholarship Committee and has served as co-chair of the IFT
      PSRP Constituency Committee and organizer of its bi-annual PSRP Constituency Conference. At the national level, Christel
      has worked on the PSRP Professional Problems Committee for the American Federation of Teachers and was a member of its
      Summer Organizing Institute in Houston, Texas. A native of Chicago’s West Side, Christel was raised by a single mother
      who retired as a school clerk. She graduated from John Marshall High School and proudly serves on the school’s alumni
      committee. Christel, her husband and her three daughters are all graduates of CPS, with her middle daughter starring for
      a Marshall Lady Commandos state championship team under legendary coach Dorothy Gaters."""
      , imgSrc = Nothing -- Just "/images/people/danyazituni.jpg"
      }
    ]


endorsements : List { name : String, imgSrc : String }
endorsements =
    [ { name = "Arab American Action Network (AAAN)"
      , imgSrc = "/images/orgs/aaan.jpg"
      }
    , { name = "Black Lives Matter (BLM) Chicago"
      , imgSrc = "/images/orgs/blmc.png"
      }
    , { name = "CODEPINK"
      , imgSrc = "/images/orgs/codepink.png"
      }
    , { name = "Human Rights Initiative (HRI) North Texas"
      , imgSrc = "/images/orgs/hrint.png"
      }
    , { name = "Chicago Teachers Union (CTU)"
      , imgSrc = "/images/orgs/ctu.png"
      }
    , { name = "Chicago Torture Justice Center"
      , imgSrc = "/images/orgs/ctjc.png"
      }
    , { name = "National Conference Black Lawyers (NCBL)"
      , imgSrc = "/images/orgs/ncbl.png"
      }
    , { name = "NAACP St. Louis, MO"
      , imgSrc = "/images/orgs/naacpsl.png"
      }
    , { name = "Anti Police-Terror Project"
      , imgSrc = "/images/orgs/aptp.png"
      }
    , { name = "Psychologists for Social Responsibility"
      , imgSrc = "/images/orgs/psysr.png"
      }
    , { name = "National Alliance for Filipino Concerns (NAFCON)"
      , imgSrc = "/images/orgs/nafcon.png"
      }
    , { name = "Organization of Black Students (OBS) University of Chicago"
      , imgSrc = "/images/orgs/obs.jpg"
      }
    , { name = "Pan-African Community Action (PACA)"
      , imgSrc = "/images/orgs/paca.png"
      }
    , { name = "Assata's Daughters"
      , imgSrc = "/images/orgs/assatasdaughters.png"
      }
    , { name = "International Women's Alliance"
      , imgSrc = "/images/orgs/iwa.png"
      }
    , { name = "Anakbayan USA"
      , imgSrc = "/images/orgs/abusa.png"
      }
    , { name = "Kentucky Alliance Against Racist & Political Repression (KAARPR)"
      , imgSrc = "/images/orgs/kaarpr.jpg"
      }
    , { name = "Southsiders Organized for Unity and Liberation"
      , imgSrc = "/images/orgs/soul.png"
      }
    , { name = "U.S. Palestinian Community Network (USPCN)"
      , imgSrc = "/images/orgs/uspcn.jpg"
      }
    , { name = "Forced Trajectory Project"
      , imgSrc = "/images/orgs/ftp.jpg"
      }
    , { name = "People's Opposition to War, Imperialism, and Racism (POWIR)"
      , imgSrc = "/images/orgs/powir.jpg"
      }
    , { name = "San Jose Nihonmachi Outreach Committee (NOC)"
      , imgSrc = "/images/orgs/noc.jpg"
      }
    , { name = "Campaign for Respect, Fairness and Human Dignity (CRF Human Dignity)"
      , imgSrc = "/images/orgs/crfhd.jpg"
      }
    , { name = "National Students for a Democratic Society (SDS)"
      , imgSrc = "/images/orgs/sds.png"
      }
    , { name = "Utah Against Police Brutality"
      , imgSrc = "/images/orgs/uapb.png"
      }
    , { name = "Legalization for All Network (L4A)"
      , imgSrc = "/images/orgs/l4a.png"
      }
    , { name = "Chicago Boricua Resistance"
      , imgSrc = "/images/orgs/cbr.jpg"
      }
    , { name = "Chicago Alliance Against Racist & Political Repression (CAARPR)"
      , imgSrc = "/images/orgs/caarpr.png"
      }
    , { name = "Coalition for Civil Freedoms"
      , imgSrc = "/images/orgs/cfcf.png"
      }
    , { name = "Migrante"
      , imgSrc = "/images/orgs/migrante.png"
      }
    , { name = "Ohio Families Unite Against Police Brutality"
      , imgSrc = "/images/orgs/ofuapb.jpg"
      }
    , { name = "Twin Cities Coalition for Justice 4 Jamar (TCCJ4J)"
      , imgSrc = "/images/orgs/tcc4j.jpg"
      }
    , { name = "Common Connections Foundation"
      , imgSrc = "/images/orgs/ccf.jpg"
      }
    , { name = "Chicago Committee for Human Rights in the Philippines (CCHRP)"
      , imgSrc = "/images/orgs/cchrp.png"
      }
    , { name = "GABRIELA-USA"
      , imgSrc = "/images/orgs/gusa.png"
      }
    , { name = "Freedom Road Socialist Organization (FRSO)"
      , imgSrc = "/images/orgs/frso.png"
      }
    , { name = "United Working Families"
      , imgSrc = "/images/orgs/uwf.webp"
      }
    , { name = "Save One Life Foundation"
      , imgSrc = "/images/orgs/solf.png"
      }
    , { name = "Veterans for Peace"
      , imgSrc = "/images/orgs/vfp.png"
      }
    , { name = "International Action Center (IAC)"
      , imgSrc = "/images/orgs/iac.jpg"
      }
    , { name = "Eastside Arts Alliance"
      , imgSrc = "/images/orgs/eaa.png"
      }
    , { name = "Women Against Military Madness"
      , imgSrc = "/images/orgs/wamm.gif"
      }
    , { name = "North Texas Action Committee (NTAC)"
      , imgSrc = "/images/orgs/ntac.png"
      }
    , { name = "Stand With Kashmir"
      , imgSrc = "/images/orgs/swk.webp"
      }
    , { name = "Minnesota Immigrant Rights Action Committee (MIRAC)"
      , imgSrc = "/images/orgs/mirac.jpg"
      }
    , { name = "U.S. Peace Council"
      , imgSrc = "/images/orgs/uspc.png"
      }
    , { name = "Malcolm X Grassroots Movement (MXGM)"
      , imgSrc = "/images/orgs/mxgm.jpg"
      }
    , { name = "Families United 4 Justice"
      , imgSrc = "/images/orgs/fu4j.jpg"
      }
    , { name = "National Jericho Movement"
      , imgSrc = "/images/orgs/njm.jpg"
      }
    , { name = "Tallahassee Community Action Committee"
      , imgSrc = "/images/orgs/tcac.png"
      }
    , { name = "Good Kids Mad City"
      , imgSrc = "/images/orgs/gkmc.jpg"
      }
    , { name = "Native Lives Matter"
      , imgSrc = "/images/orgs/nlm.jpg"
      }
    , { name = "Centro CSO"
      , imgSrc = "/images/orgs/ccso.jpg"
      }
    , { name = "Jacksonville Community Action Committee (JCAC)"
      , imgSrc = "/images/orgs/jcac.jpg"
      }
    , { name = "Pilsen Alliance"
      , imgSrc = "/images/orgs/pilsenalliance.jpg"
      }
    , { name = "Women’s All Point Bulletin"
      , imgSrc = "/images/orgs/wapb.jpg"
      }
    ]


type alias Session =
    { title : String
    , sessionDescription : String
    , sessionLocation : String
    , people :
        { speakers : Maybe (List String)
        , moderators : Maybe (List String)
        }
    }


type alias AgendaSlot =
    { slotTime : String
    , slotType : String
    , sessions : List Session
    }


agenda : List { day : String, slots : List AgendaSlot }
agenda =
    [ { day = "Friday, November 22"
      , slots =
            [ { slotTime = "6:00 PM"
              , slotType = ""
              , sessions =
                    [ { title = "Doors Open"
                      , sessionDescription = ""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "7:00 PM"
              , slotType = ""
              , sessions =
                    [ { title = "Rally On Human Rights"
                      , sessionDescription = "Defending Democracy and Community Control of the Police. Keynote address by Angela Y. Davis."
                      , sessionLocation = "Main Hall"
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            ]
      }
    , { day = "Saturday, November 23"
      , slots =
            [ { slotTime = "8:30 AM"
              , slotType = ""
              , sessions =
                    [ { title = "Doors Open"
                      , sessionDescription = "Registration and continental breakfast."
                      , sessionLocation = "Main Hall"
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "9:30 AM"
              , slotType = ""
              , sessions =
                    [ { title = "Welcome"
                      , sessionDescription = "Opening Resolution: Refound the National Alliance Against Racist and Political Repression!"
                      , sessionLocation = "Main Hall"
                      , people =
                            { speakers = Just ["Christel Williams-Hayes"]
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "10:00 AM"
              , slotType = "Plenary"
              , sessions =
                    [ { title = "[PANEL] The struggle for police accountability in a prison nation must be based on a strategy for community control of the police."
                      , sessionDescription =
                            """This plenary will educate explain the need for a movement for community control of the police as well as
ending mass incarceration in the United States. Audience members will learn about the historical demand for
community control of the police, it’s origins and why the movement to end police crimes must fight for
community control of the people."""
                      , sessionLocation = "Main Hall"
                      , people =
                            { speakers =
                                Just
                                    [ "Frank Chapman"
                                    , "Angela Davis"
                                    , "Mike Sampson"
                                    , "Tanya Watkins"
                                    ]
                            , moderators = Just [ "Loretta VanPelt" ]
                            }
                      }
                    ]
              }
            , { slotTime = "11:00 AM"
              , slotType = "Plenary"
              , sessions =
                    [ { title = "[PANEL] People's Movements Under Attack: Fighting Back"
                      , sessionDescription = """A panel of organizers from oppressed nationality and labor movements will discuss how the government has
suppressed the people's movements, and how the movements - including the National Alliance since its
founding - are fighting back."""
                      , sessionLocation = "Main Hall"
                      , people =
                            { speakers =
                                Just
                                    [ "Danya Zituni"
                                    , "Ralph Poynter"
                                    , "Cherrene Horazuk"
                                    , "Masao Suzuki"
                                    , "Mildred Williamson"
                                    , "Emma Lozano"
                                    ]
                            , moderators = Just [ "Ariel Atkins" ]
                            }
                      }
                    ]
              }
            , { slotTime = "12:00 PM"
              , slotType = ""
              , sessions =
                    [ { title = "Lunch"
                      , sessionDescription = ""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "1:00 PM"
              , slotType = "Breakout Session"
              , sessions =
                    [ { title = "[WORKSHOP] How to build for community control of the police: Lessons and Strategies from the movement."
                      , sessionDescription = """The purpose of this workshop is to share experiences of organizing for community control of the police
grounded in the practical organizing experience of campaigns from different cities. This workshop is an
opportunity for groups to exchange ideas on how to do better their local police accountability work while
drawing lessons from existing movements."""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators =
                                Just
                                    [ "Michael Sampson", "Sydney Loving" ]
                            }
                      }
                    , { title = "[WORKSHOP] It is our duty to fight for our freedom: The fight to free political prisoners and the wrongfully-convicted, and to resist political repression"
                      , sessionDescription = """A panel of former political prisoners, grand jury resisters, and those working in their defense will speak about
their experiences. Join the fight to free political prisoners and prisoners of war, and discuss how we can resist
government attacks on our movements today."""
                      , sessionLocation = ""
                      , people =
                            { speakers =
                                Just
                                    [ "Devin Branch"
                                    , "Betty Davis"
                                    , "Masai Ehehosi"
                                    , "LaTanya Jenifor-Sublett"
                                    , "Bassem Kawar"
                                    , "Carlos Montes"
                                    , "Rhonda Ramiro"
                                    , "Tracy Molm"
                                    , "Sheridan Murphy"
                                    , "Regina Russell"
                                    ]
                            , moderators =
                                Just
                                    [ "hondo t’chikwa"
                                    , "Jess Sundin"
                                    ]
                            }
                      }
                    , { title = "[WORKSHOP] The fight against racist and anti-LGBT/Queer violence."
                      , sessionDescription = """This workshop will focus on different struggles such as the movement against white supremacy/white
nationalism along with the struggle against violence imposed on the LGBTQI, specifically the violence against
transwomen of color. This workshop as well will uplift struggles against legislation police policies which
contribute to racist and anti-queer violence. It will also highlight how capitalism’s creation of gender roles has
led to the police upholding a structure that targets any individuals living outside of those strict gender roles as
criminal or deviant."""
                      , sessionLocation = ""
                      , people =
                            { speakers =
                                Just
                                    [ "CeCe McDonald"
                                    , "Kent Marrero"
                                    , "Stephanie Skora"
                                    , "Mia Whatley"
                                    , "more TBA"
                                    ]
                            , moderators = Just [ "Christina Kittle" ]
                            }
                      }
                    , { title = "[WORKSHOP] Families to the front! Families as leaders in the fight against police murder and unjust incarceration"
                      , sessionDescription = """Families will share their loved ones’ stories, then discuss with fellow activists will address how they’ve worked
together to fight for justice. Attendees should bring questions and ideas for how to best support families and
build their activism."""
                      , sessionLocation = ""
                      , people =
                            { speakers =
                                Just
                                    [ "Dinni Aden"
                                    , "Sumaya Aden"
                                    , "Monique Cullars Doty"
                                    , "Bertha Escamilla"
                                    , "Toshira Garraway"
                                    , "Kimberly Handy-Jones"
                                    , "Dorothy Holmes"
                                    , "Esther Hernandez"
                                    , "Shirley McDaniels"
                                    , "Marvin Oliveros"
                                    , "Arewa Karen Winters"
                                    , "others TBA"
                                    ]
                            , moderators =
                                Just
                                    [ "Aislinn Pulley"
                                    , "Regina Russell"
                                    , "Jess Sundin"
                                    ]
                            }
                      }
                    ]
              }
            , { slotTime = "2:30 PM"
              , slotType = ""
              , sessions =
                    [ { title = "Break"
                      , sessionDescription = ""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "2:45 PM"
              , slotType = "Breakout Session"
              , sessions =
                    [ { title = "[WORKSHOP] How to build for community control of the police: Lessons and Strategies from the movement."
                      , sessionDescription = """The purpose of this workshop is to share experiences of organizing for community control of the police
grounded in the practical organizing experience of campaigns from different cities. This workshop is an
opportunity for groups to exchange ideas on how to do better their local police accountability work while
drawing lessons from existing movements."""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators =
                                Just
                                    [ "Michael Sampson"
                                    , "Sydney Loving"
                                    ]
                            }
                      }
                    , { title = "[WORKSHOP] It is our duty to fight for our freedom: The fight to free political prisoners and the wrongfully-convicted, and to resist political repression"
                      , sessionDescription = """A panel of former political prisoners, grand jury resisters, and those working in their defense will speak about
their experiences. Join the fight to free political prisoners and prisoners of war, and discuss how we can resist
government attacks on our movements today."""
                      , sessionLocation = ""
                      , people =
                            { speakers =
                                Just
                                    [ "Devin Branch"
                                    , "Betty Davis"
                                    , "Masai Ehehosi"
                                    , "LaTanya Jenifor-Sublett"
                                    , "Bassem Kawar"
                                    , "Carlos Montes"
                                    , "Rhonda Ramiro"
                                    , "Tracy Molm"
                                    , "Sheridan Murphy"
                                    , "Regina Russell"
                                    ]
                            , moderators =
                                Just
                                    [ "hondo t’chikwa"
                                    , "Jess Sundin"
                                    ]
                            }
                      }
                    , { title = "[WORKSHOP] Opposing police cooperation with Federal agencies to oppress our communities"
                      , sessionDescription = """Discuss how the Federal government uses local police to carry out repression against immigrants, Arabs,
Muslims, and others and ways that communities have resisted."""
                      , sessionLocation = ""
                      , people =
                            { speakers =
                                Just
                                    [ "Emma Lozano"
                                    , "Muhammad Sankari"
                                    , "Sue Udry"
                                    , "a speaker from the Minnesota Immigrants Rights Action Committee (MIRAC)"
                                    ]
                            , moderators = Just [ "Masao Suzuki" ]
                            }
                      }
                    , { title = "[WORKSHOP] Families to the front! Families as leaders in the fight against police murder and unjust incarceration"
                      , sessionDescription = """Families will share their loved ones’ stories, then discuss with fellow activists will address how they’ve worked
together to fight for justice. Attendees should bring questions and ideas for how to best support families and
build their activism."""
                      , sessionLocation = ""
                      , people =
                            { speakers =
                                Just
                                    [ "Dinni Aden"
                                    , "Sumaya Aden"
                                    , "Monique Cullars Doty"
                                    , "Bertha Escamilla"
                                    , "Toshira Garraway"
                                    , "Kimberly Handy-Jones"
                                    , "Dorothy Holmes"
                                    , "Esther Hernandez"
                                    , "Shirley McDaniels"
                                    , "Marvin Oliveros"
                                    , "Arewa Karen Winters"
                                    , "others TBA"
                                    ]
                            , moderators =
                                Just
                                    [ "Aislinn Pulley"
                                    , "Regina Russell"
                                    , "Jess Sundin"
                                    ]
                            }
                      }
                    ]
              }
            , { slotTime = "4:15 PM"
              , slotType = ""
              , sessions =
                    [ { title = "Break"
                      , sessionDescription = ""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "4:30 PM"
              , slotType = "Plenary"
              , sessions =
                    [ { title = "[PANEL] Lessons from the Struggle"
                      , sessionDescription = """The purpose of this plenary is to educate audience members on the need to contextualize lessons from
people’s movements in order to understand how we can continue to successfully fight against white
supremacy and capitalism."""
                      , sessionLocation = "Main Hall"
                      , people =
                            { speakers =
                                Just
                                    [ "Frank Chapman"
                                    , "Angela Davis"
                                    , "Jazmine Salas"
                                    , "Jess Sundin"
                                    , "Marisol Marquez"
                                    , "USPCN rep"
                                    ]
                            , moderators = Just [ "Monique Sampson" ]
                            }
                      }
                    ]
              }
            , { slotTime = "6:00 PM"
              , slotType = ""
              , sessions =
                    [ { title = "Recess"
                      , sessionDescription = "Followed by dinner and social gathering."
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            ]
      }
    , { day = "Sunday, November 24"
      , slots =
            [ { slotTime = "9:00 AM"
              , slotType = ""
              , sessions =
                    [ { title = "Continental Breakfast"
                      , sessionDescription = ""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "10:00 AM"
              , slotType = "Plenary"
              , sessions =
                    [ { title = "Final Session"
                      , sessionDescription = "Resolution to Refound the National Alliance: Election of Leadership, Report Back from Workshops."
                      , sessionLocation = "Main Hall"
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            , { slotTime = "12:00 PM"
              , slotType = "Conference Adjourns"
              , sessions =
                    [ { title = "Conference Adjourns"
                      , sessionDescription = ""
                      , sessionLocation = ""
                      , people =
                            { speakers = Nothing
                            , moderators = Nothing
                            }
                      }
                    ]
              }
            ]
      }
    ]
